/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

// Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('chat-messages', () => import('./components/ChatMessages.vue'));
// Vue.component('chat-form', () => import('./components/ChatForm.vue'));
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    data: {
        messages: []
        // #data
    },
    mounted() {
        // #mounted
    },
    methods: {
        // #methods
        // addMessage(message) {
        //     this.messages.push(message);

        //     axios.post('/messages', message).then(response => {
        //         console.log(response.data);
        //     });
        // },
        pushmessage(message) {
            this.messages.push(message);
        },
        // replacemessage(message) {
        //     this.messages = message;
        // }
    },
    watch: {
        // #watch
    }
});

var id = Vue.prototype.$userId = document.querySelector("meta[name='user-id']").getAttribute('content');
var admin = Vue.prototype.$useradmin = document.querySelector("meta[name='admin']").getAttribute('content');
//     <script>
//     Echo.private('chat')
//     .listen('MessageSent', (e) => {
//         if(e.message.ticket_id == {{ $ticket->id }}){
//             this.messages.push({
//                 message: e.message.message,
//                 user: e.user
//             });
//         }
//     });
// </script>

function notifyMe(title, text) {

    var options = {
        body: text,
        // icon,
        // data: { url }, //the url which we gonna use later
        // actions: [{action: "open_url", title: "Read Now"}]
    }

    // Let's check if the browser supports notifications
    if (!("Notification" in window)) {
        // alert("This browser does not support desktop notification");
    }

    // Let's check whether notification permissions have already been granted
    else if (Notification.permission === "granted") {
        // If it's okay let's create a notification
        var notification = new Notification(title, options);
        // var notification = new Notification(text);
    }

    // Otherwise, we need to ask the user for permission
    else if (Notification.permission !== "denied") {
        Notification.requestPermission().then(function (permission) {
            // If the user accepts, let's create a notification
            // console.log("asdas");
            if (permission === "granted") {
                var notification = new Notification(title, options);
                // var notification = new Notification(text);
            }
        });
    }

    // At last, if the user has denied notifications, and you
    // want to be respectful there is no need to bother them any more.
}

function push(title, message, ticket, id_ticket) {
    if (("Website2APK" in window)) {
        token = Website2APK.getFirebaseDeviceToken();
        var page = [
            "ticket_kesehatan",
            "ticket_pengobatan",
            "panic",
        ]
    
        axios.post("https://websitetoapk.com/pushadmin/api/v1/send_individual",
            {
                "packageName": "com.hastabratacare.batubhayangkara",
                "type": "Simple",
                "title": title,
                "description": message,
                // "image": "https://static01.nyt.com/images/2021/04/03/multimedia/03xp-april/merlin_185893383_8e41433f-4a32-4b1e-bf02-457290d0d534-superJumbo.jpg",
                "url": "http://hastacare.jauhdiatas.xyz/" + page[ticket] + "/" + id_ticket,
                "ring": "RING",
                "deviceToken": token
            },
            {
                headers: {
                    "Content-Type": 'application/x-www-form-urlencoded',
                    "X-Auth-Token": '251ea5bc5b020d18a33fc1d60baadd91',
                    "X-Api-Key": 'S5GW8uozsapDOqT5lpBtU2QtnGLanxXPUJSKJaOMcmc'
                }
            }).then((response) => {
                console.log(response.data);
            });
    }

}

Echo.private('chat')
    .listen('MessageSent', (e) => {
        console.log(e.user.id, e.ticket.user_id, id, admin);
        if (admin == 1) {
            if (e.user.admin == 0) {
                notifyMe('Pesan Masuk', e.message.message)
                push('Pesan Masuk', e.message.message, e.ticket.type, e.ticket.id)
            }
        }
        else {
            if (e.ticket.user_id == id && e.user.id != id) {
                notifyMe('Pesan Masuk', e.message.message)
                push('Pesan Masuk', e.message.message, e.ticket.type, e.ticket.id)
            }
        }
    })
    .listen('PanicSent', (e) => {
        console.log("sos")
        if (admin == 1) {
            console.log("Panic")
            notifyMe('SOS', "Nama:" + e.user.name + "\nDinas:" + e.user.dinas)
            play()
            push('SOS', "Nama:" + e.user.name + "\nDinas:" + e.user.dinas, 2, '')
        }
    });

function play() {
    var audio = document.getElementById("sos_audio");
    audio.play();
}

// Echo.private('panic_notif').listen('PanicSent', (e) => {
//     console.log("sos")
//     if (admin == 1) {
//         console.log("Panic")
//         notifyMe('SOS', "SOS")
//         play()
//     }
// });
