@extends('layouts.app')

@section('content')
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">Riwayat {{ $riwayat->id }}</div>
            <div class="card-body">

                <a href="{{ url('/profil_kesehatan') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                <a href="{{ url('/profil_kesehatan/' . $riwayat->id . '/edit') }}" title="Edit Riwayat"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                <form method="POST" action="{{ url('profil_kesehatan' . '/' . $riwayat->id) }}" accept-charset="UTF-8" style="display:inline">
                    {{ method_field('DELETE') }}
                    {{ csrf_field() }}
                    <button type="submit" class="btn btn-danger btn-sm" title="Delete Riwayat" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                </form>
                <br/>
                <br/>

                <div class="table-responsive">
                    <table class="table">
                        <tbody>
                            <tr>
                                <th>ID</th><td>{{ $riwayat->id }}</td>
                            </tr>
                            <tr><th> User Id </th><td> {{ $riwayat->user_id }} </td></tr><tr><th> Judul </th><td> {{ $riwayat->judul }} </td></tr><tr><th> Deskripsi </th><td> {{ $riwayat->deskripsi }} </td></tr>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
@endsection
