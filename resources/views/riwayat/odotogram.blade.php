<div id="svgselect" style="width: 610px; height: 350px;">
    <!-- background-color:red -->
    <svg version="1.1" height="100%" width="100%">
        <g transform="scale(1.5)" id="gmain">
            <g id="P18" transform="translate(0,0)">
                <polygon points="5,5 	15,5 	15,15 	5,15" fill="{{ $odontogram[18] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="C"
                    opacity="1"></polygon>
                <polygon points="0,0 	20,0 	15,5 	5,5" fill="{{ $odontogram[18] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="T" opacity="1">
                </polygon>
                <polygon points="5,15 	15,15 	20,20 	0,20" fill="{{ $odontogram[18] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="B"
                    opacity="1"></polygon>
                <polygon points="15,5 	20,0 	20,20 	15,15" fill="{{ $odontogram[18] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="R"
                    opacity="1"></polygon>
                <polygon points="0,0 	5,5 	5,15 	0,20" fill="{{ $odontogram[18] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="L" opacity="1">
                </polygon>
                <text x="6" y="30" stroke="navy" fill="navy" stroke-width="0.1"
                    style="font-size: 6pt;font-weight:normal">18</text>
            </g>
            <g id="P17" transform="translate(25,0)">
                <polygon points="5,5 	15,5 	15,15 	5,15" fill="{{ $odontogram[17] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="C"
                    opacity="1"></polygon>
                <polygon points="0,0 	20,0 	15,5 	5,5" fill="{{ $odontogram[17] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="T" opacity="1">
                </polygon>
                <polygon points="5,15 	15,15 	20,20 	0,20" fill="{{ $odontogram[17] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="B"
                    opacity="1"></polygon>
                <polygon points="15,5 	20,0 	20,20 	15,15" fill="{{ $odontogram[17] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="R"
                    opacity="1"></polygon>
                <polygon points="0,0 	5,5 	5,15 	0,20" fill="{{ $odontogram[17] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="L" opacity="1">
                </polygon>
                <text x="6" y="30" stroke="navy" fill="navy" stroke-width="0.1"
                    style="font-size: 6pt;font-weight:normal">17</text>
            </g>
            <g id="P16" transform="translate(50,0)">
                <polygon points="5,5 	15,5 	15,15 	5,15" fill="{{ $odontogram[16] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="C"
                    opacity="1"></polygon>
                <polygon points="0,0 	20,0 	15,5 	5,5" fill="{{ $odontogram[16] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="T" opacity="1">
                </polygon>
                <polygon points="5,15 	15,15 	20,20 	0,20" fill="{{ $odontogram[16] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="B"
                    opacity="1"></polygon>
                <polygon points="15,5 	20,0 	20,20 	15,15" fill="{{ $odontogram[16] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="R"
                    opacity="1"></polygon>
                <polygon points="0,0 	5,5 	5,15 	0,20" fill="{{ $odontogram[16] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="L" opacity="1">
                </polygon>
                <text x="6" y="30" stroke="navy" fill="navy" stroke-width="0.1"
                    style="font-size: 6pt;font-weight:normal">16</text>
            </g>
            <g id="P15" transform="translate(75,0)">
                <polygon points="5,5 	15,5 	15,15 	5,15" fill="{{ $odontogram[15] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="C"
                    opacity="1"></polygon>
                <polygon points="0,0 	20,0 	15,5 	5,5" fill="{{ $odontogram[15] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="T" opacity="1">
                </polygon>
                <polygon points="5,15 	15,15 	20,20 	0,20" fill="{{ $odontogram[15] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="B"
                    opacity="1"></polygon>
                <polygon points="15,5 	20,0 	20,20 	15,15" fill="{{ $odontogram[15] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="R"
                    opacity="1"></polygon>
                <polygon points="0,0 	5,5 	5,15 	0,20" fill="{{ $odontogram[15] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="L" opacity="1">
                </polygon>
                <text x="6" y="30" stroke="navy" fill="navy" stroke-width="0.1"
                    style="font-size: 6pt;font-weight:normal">15</text>
            </g>
            <g id="P14" transform="translate(100,0)">
                <polygon points="5,5 	15,5 	15,15 	5,15" fill="{{ $odontogram[14] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="C"
                    opacity="1"></polygon>
                <polygon points="0,0 	20,0 	15,5 	5,5" fill="{{ $odontogram[14] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="T" opacity="1">
                </polygon>
                <polygon points="5,15 	15,15 	20,20 	0,20" fill="{{ $odontogram[14] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="B"
                    opacity="1"></polygon>
                <polygon points="15,5 	20,0 	20,20 	15,15" fill="{{ $odontogram[14] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="R"
                    opacity="1"></polygon>
                <polygon points="0,0 	5,5 	5,15 	0,20" fill="{{ $odontogram[14] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="L" opacity="1">
                </polygon>
                <text x="6" y="30" stroke="navy" fill="navy" stroke-width="0.1"
                    style="font-size: 6pt;font-weight:normal">14</text>
            </g>
            <g id="P13" transform="translate(125,0)">
                <polygon points="5,5 	15,5 	15,15 	5,15" fill="{{ $odontogram[13] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="C"
                    opacity="1"></polygon>
                <polygon points="0,0 	20,0 	15,5 	5,5" fill="{{ $odontogram[13] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="T" opacity="1">
                </polygon>
                <polygon points="5,15 	15,15 	20,20 	0,20" fill="{{ $odontogram[13] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="B"
                    opacity="1"></polygon>
                <polygon points="15,5 	20,0 	20,20 	15,15" fill="{{ $odontogram[13] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="R"
                    opacity="1"></polygon>
                <polygon points="0,0 	5,5 	5,15 	0,20" fill="{{ $odontogram[13] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="L" opacity="1">
                </polygon>
                <text x="6" y="30" stroke="navy" fill="navy" stroke-width="0.1"
                    style="font-size: 6pt;font-weight:normal">13</text>
            </g>
            <g id="P12" transform="translate(150,0)">
                <polygon points="5,5 	15,5 	15,15 	5,15" fill="{{ $odontogram[12] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="C"
                    opacity="1"></polygon>
                <polygon points="0,0 	20,0 	15,5 	5,5" fill="{{ $odontogram[12] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="T" opacity="1">
                </polygon>
                <polygon points="5,15 	15,15 	20,20 	0,20" fill="{{ $odontogram[12] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="B"
                    opacity="1"></polygon>
                <polygon points="15,5 	20,0 	20,20 	15,15" fill="{{ $odontogram[12] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="R"
                    opacity="1"></polygon>
                <polygon points="0,0 	5,5 	5,15 	0,20" fill="{{ $odontogram[12] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="L" opacity="1">
                </polygon>
                <text x="6" y="30" stroke="navy" fill="navy" stroke-width="0.1"
                    style="font-size: 6pt;font-weight:normal">12</text>
            </g>
            <g id="P11" transform="translate(175,0)">
                <polygon points="5,5 	15,5 	15,15 	5,15" fill="{{ $odontogram[11] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="C"
                    opacity="1"></polygon>
                <polygon points="0,0 	20,0 	15,5 	5,5" fill="{{ $odontogram[11] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="T" opacity="1">
                </polygon>
                <polygon points="5,15 	15,15 	20,20 	0,20" fill="{{ $odontogram[11] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="B"
                    opacity="1"></polygon>
                <polygon points="15,5 	20,0 	20,20 	15,15" fill="{{ $odontogram[11] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="R"
                    opacity="1"></polygon>
                <polygon points="0,0 	5,5 	5,15 	0,20" fill="{{ $odontogram[11] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="L" opacity="1">
                </polygon>
                <text x="6" y="30" stroke="navy" fill="navy" stroke-width="0.1"
                    style="font-size: 6pt;font-weight:normal">11</text>
            </g>

            <!-- Baris kedua -->

            <g id="P55" transform="translate(75,40)">
                <polygon points="5,5 	15,5 	15,15 	5,15" fill="{{ $odontogram[55] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="C"
                    opacity="1"></polygon>
                <polygon points="0,0 	20,0 	15,5 	5,5" fill="{{ $odontogram[55] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="T" opacity="1">
                </polygon>
                <polygon points="5,15 	15,15 	20,20 	0,20" fill="{{ $odontogram[55] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="B"
                    opacity="1"></polygon>
                <polygon points="15,5 	20,0 	20,20 	15,15" fill="{{ $odontogram[55] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="R"
                    opacity="1"></polygon>
                <polygon points="0,0 	5,5 	5,15 	0,20" fill="{{ $odontogram[55] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="L" opacity="1">
                </polygon>
                <text x="6" y="30" stroke="navy" fill="navy" stroke-width="0.1"
                    style="font-size: 6pt;font-weight:normal">55</text>
            </g>
            <g id="P54" transform="translate(100,40)">
                <polygon points="5,5 	15,5 	15,15 	5,15" fill="{{ $odontogram[54] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="C"
                    opacity="1"></polygon>
                <polygon points="0,0 	20,0 	15,5 	5,5" fill="{{ $odontogram[54] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="T" opacity="1">
                </polygon>
                <polygon points="5,15 	15,15 	20,20 	0,20" fill="{{ $odontogram[54] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="B"
                    opacity="1"></polygon>
                <polygon points="15,5 	20,0 	20,20 	15,15" fill="{{ $odontogram[54] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="R"
                    opacity="1"></polygon>
                <polygon points="0,0 	5,5 	5,15 	0,20" fill="{{ $odontogram[54] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="L" opacity="1">
                </polygon>
                <text x="6" y="30" stroke="navy" fill="navy" stroke-width="0.1"
                    style="font-size: 6pt;font-weight:normal">54</text>
            </g>
            <g id="P53" transform="translate(125,40)">
                <polygon points="5,5 	15,5 	15,15 	5,15" fill="{{ $odontogram[53] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="C"
                    opacity="1"></polygon>
                <polygon points="0,0 	20,0 	15,5 	5,5" fill="{{ $odontogram[53] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="T" opacity="1">
                </polygon>
                <polygon points="5,15 	15,15 	20,20 	0,20" fill="{{ $odontogram[53] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="B"
                    opacity="1"></polygon>
                <polygon points="15,5 	20,0 	20,20 	15,15" fill="{{ $odontogram[53] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="R"
                    opacity="1"></polygon>
                <polygon points="0,0 	5,5 	5,15 	0,20" fill="{{ $odontogram[53] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="L" opacity="1">
                </polygon>
                <text x="6" y="30" stroke="navy" fill="navy" stroke-width="0.1"
                    style="font-size: 6pt;font-weight:normal">53</text>
            </g>
            <g id="P52" transform="translate(150,40)">
                <polygon points="5,5 	15,5 	15,15 	5,15" fill="{{ $odontogram[52] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="C"
                    opacity="1"></polygon>
                <polygon points="0,0 	20,0 	15,5 	5,5" fill="{{ $odontogram[52] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="T" opacity="1">
                </polygon>
                <polygon points="5,15 	15,15 	20,20 	0,20" fill="{{ $odontogram[52] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="B"
                    opacity="1"></polygon>
                <polygon points="15,5 	20,0 	20,20 	15,15" fill="{{ $odontogram[52] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="R"
                    opacity="1"></polygon>
                <polygon points="0,0 	5,5 	5,15 	0,20" fill="{{ $odontogram[52] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="L" opacity="1">
                </polygon>
                <text x="6" y="30" stroke="navy" fill="navy" stroke-width="0.1"
                    style="font-size: 6pt;font-weight:normal">52</text>
            </g>
            <g id="P51" transform="translate(175,40)">
                <polygon points="5,5 	15,5 	15,15 	5,15" fill="{{ $odontogram[51] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="C"
                    opacity="1"></polygon>
                <polygon points="0,0 	20,0 	15,5 	5,5" fill="{{ $odontogram[51] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="T" opacity="1">
                </polygon>
                <polygon points="5,15 	15,15 	20,20 	0,20" fill="{{ $odontogram[51] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="B"
                    opacity="1"></polygon>
                <polygon points="15,5 	20,0 	20,20 	15,15" fill="{{ $odontogram[51] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="R"
                    opacity="1"></polygon>
                <polygon points="0,0 	5,5 	5,15 	0,20" fill="{{ $odontogram[51] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="L" opacity="1">
                </polygon>
                <text x="6" y="30" stroke="navy" fill="navy" stroke-width="0.1"
                    style="font-size: 6pt;font-weight:normal">51</text>
            </g>

            <!-- row ke tiga baris pertama -->

            <g id="P85" transform="translate(75,80)">
                <polygon points="5,5 	15,5 	15,15 	5,15" fill="{{ $odontogram[85] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="C"
                    opacity="1"></polygon>
                <polygon points="0,0 	20,0 	15,5 	5,5" fill="{{ $odontogram[85] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="T" opacity="1">
                </polygon>
                <polygon points="5,15 	15,15 	20,20 	0,20" fill="{{ $odontogram[85] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="B"
                    opacity="1"></polygon>
                <polygon points="15,5 	20,0 	20,20 	15,15" fill="{{ $odontogram[85] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="R"
                    opacity="1"></polygon>
                <polygon points="0,0 	5,5 	5,15 	0,20" fill="{{ $odontogram[85] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="L" opacity="1">
                </polygon>
                <text x="6" y="30" stroke="navy" fill="navy" stroke-width="0.1"
                    style="font-size: 6pt;font-weight:normal">85</text>
            </g>
            <g id="P84" transform="translate(100,80)">
                <polygon points="5,5 	15,5 	15,15 	5,15" fill="{{ $odontogram[84] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="C"
                    opacity="1"></polygon>
                <polygon points="0,0 	20,0 	15,5 	5,5" fill="{{ $odontogram[84] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="T" opacity="1">
                </polygon>
                <polygon points="5,15 	15,15 	20,20 	0,20" fill="{{ $odontogram[84] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="B"
                    opacity="1"></polygon>
                <polygon points="15,5 	20,0 	20,20 	15,15" fill="{{ $odontogram[84] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="R"
                    opacity="1"></polygon>
                <polygon points="0,0 	5,5 	5,15 	0,20" fill="{{ $odontogram[84] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="L" opacity="1">
                </polygon>
                <text x="6" y="30" stroke="navy" fill="navy" stroke-width="0.1"
                    style="font-size: 6pt;font-weight:normal">84</text>
            </g>
            <g id="P83" transform="translate(125,80)">
                <polygon points="5,5 	15,5 	15,15 	5,15" fill="{{ $odontogram[83] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="C"
                    opacity="1"></polygon>
                <polygon points="0,0 	20,0 	15,5 	5,5" fill="{{ $odontogram[83] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="T" opacity="1">
                </polygon>
                <polygon points="5,15 	15,15 	20,20 	0,20" fill="{{ $odontogram[83] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="B"
                    opacity="1"></polygon>
                <polygon points="15,5 	20,0 	20,20 	15,15" fill="{{ $odontogram[83] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="R"
                    opacity="1"></polygon>
                <polygon points="0,0 	5,5 	5,15 	0,20" fill="{{ $odontogram[83] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="L" opacity="1">
                </polygon>
                <text x="6" y="30" stroke="navy" fill="navy" stroke-width="0.1"
                    style="font-size: 6pt;font-weight:normal">83</text>
            </g>
            <g id="P82" transform="translate(150,80)">
                <polygon points="5,5 	15,5 	15,15 	5,15" fill="{{ $odontogram[82] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="C"
                    opacity="1"></polygon>
                <polygon points="0,0 	20,0 	15,5 	5,5" fill="{{ $odontogram[82] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="T" opacity="1">
                </polygon>
                <polygon points="5,15 	15,15 	20,20 	0,20" fill="{{ $odontogram[82] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="B"
                    opacity="1"></polygon>
                <polygon points="15,5 	20,0 	20,20 	15,15" fill="{{ $odontogram[82] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="R"
                    opacity="1"></polygon>
                <polygon points="0,0 	5,5 	5,15 	0,20" fill="{{ $odontogram[82] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="L" opacity="1">
                </polygon>
                <text x="6" y="30" stroke="navy" fill="navy" stroke-width="0.1"
                    style="font-size: 6pt;font-weight:normal">82</text>
            </g>
            <g id="P81" transform="translate(175,80)">
                <polygon points="5,5 	15,5 	15,15 	5,15" fill="{{ $odontogram[81] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="C"
                    opacity="1"></polygon>
                <polygon points="0,0 	20,0 	15,5 	5,5" fill="{{ $odontogram[81] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="T" opacity="1">
                </polygon>
                <polygon points="5,15 	15,15 	20,20 	0,20" fill="{{ $odontogram[81] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="B"
                    opacity="1"></polygon>
                <polygon points="15,5 	20,0 	20,20 	15,15" fill="{{ $odontogram[81] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="R"
                    opacity="1"></polygon>
                <polygon points="0,0 	5,5 	5,15 	0,20" fill="{{ $odontogram[81] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="L" opacity="1">
                </polygon>
                <text x="6" y="30" stroke="navy" fill="navy" stroke-width="0.1"
                    style="font-size: 6pt;font-weight:normal">81</text>
            </g>

            <!-- row 4 baris pertaman -->

            <g id="P48" transform="translate(0,120)">
                <polygon points="5,5 	15,5 	15,15 	5,15" fill="{{ $odontogram[48] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="C"
                    opacity="1"></polygon>
                <polygon points="0,0 	20,0 	15,5 	5,5" fill="{{ $odontogram[48] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="T" opacity="1">
                </polygon>
                <polygon points="5,15 	15,15 	20,20 	0,20" fill="{{ $odontogram[48] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="B"
                    opacity="1"></polygon>
                <polygon points="15,5 	20,0 	20,20 	15,15" fill="{{ $odontogram[48] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="R"
                    opacity="1"></polygon>
                <polygon points="0,0 	5,5 	5,15 	0,20" fill="{{ $odontogram[48] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="L" opacity="1">
                </polygon>
                <text x="6" y="30" stroke="navy" fill="navy" stroke-width="0.1"
                    style="font-size: 6pt;font-weight:normal">48</text>
            </g>
            <g id="P47" transform="translate(25,120)">
                <polygon points="5,5 	15,5 	15,15 	5,15" fill="{{ $odontogram[47] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="C"
                    opacity="1"></polygon>
                <polygon points="0,0 	20,0 	15,5 	5,5" fill="{{ $odontogram[47] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="T" opacity="1">
                </polygon>
                <polygon points="5,15 	15,15 	20,20 	0,20" fill="{{ $odontogram[47] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="B"
                    opacity="1"></polygon>
                <polygon points="15,5 	20,0 	20,20 	15,15" fill="{{ $odontogram[47] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="R"
                    opacity="1"></polygon>
                <polygon points="0,0 	5,5 	5,15 	0,20" fill="{{ $odontogram[47] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="L" opacity="1">
                </polygon>
                <text x="6" y="30" stroke="navy" fill="navy" stroke-width="0.1"
                    style="font-size: 6pt;font-weight:normal">47</text>
            </g>
            <g id="P46" transform="translate(50,120)">
                <polygon points="5,5 	15,5 	15,15 	5,15" fill="{{ $odontogram[46] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="C"
                    opacity="1"></polygon>
                <polygon points="0,0 	20,0 	15,5 	5,5" fill="{{ $odontogram[46] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="T" opacity="1">
                </polygon>
                <polygon points="5,15 	15,15 	20,20 	0,20" fill="{{ $odontogram[46] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="B"
                    opacity="1"></polygon>
                <polygon points="15,5 	20,0 	20,20 	15,15" fill="{{ $odontogram[46] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="R"
                    opacity="1"></polygon>
                <polygon points="0,0 	5,5 	5,15 	0,20" fill="{{ $odontogram[46] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="L" opacity="1">
                </polygon>
                <text x="6" y="30" stroke="navy" fill="navy" stroke-width="0.1"
                    style="font-size: 6pt;font-weight:normal">46</text>
            </g>
            <g id="P45" transform="translate(75,120)">
                <polygon points="5,5 	15,5 	15,15 	5,15" fill="{{ $odontogram[45] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="C"
                    opacity="1"></polygon>
                <polygon points="0,0 	20,0 	15,5 	5,5" fill="{{ $odontogram[45] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="T" opacity="1">
                </polygon>
                <polygon points="5,15 	15,15 	20,20 	0,20" fill="{{ $odontogram[45] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="B"
                    opacity="1"></polygon>
                <polygon points="15,5 	20,0 	20,20 	15,15" fill="{{ $odontogram[45] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="R"
                    opacity="1"></polygon>
                <polygon points="0,0 	5,5 	5,15 	0,20" fill="{{ $odontogram[45] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="L" opacity="1">
                </polygon>
                <text x="6" y="30" stroke="navy" fill="navy" stroke-width="0.1"
                    style="font-size: 6pt;font-weight:normal">45</text>
            </g>
            <g id="P44" transform="translate(100,120)">
                <polygon points="5,5 	15,5 	15,15 	5,15" fill="{{ $odontogram[44] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="C"
                    opacity="1"></polygon>
                <polygon points="0,0 	20,0 	15,5 	5,5" fill="{{ $odontogram[44] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="T" opacity="1">
                </polygon>
                <polygon points="5,15 	15,15 	20,20 	0,20" fill="{{ $odontogram[44] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="B"
                    opacity="1"></polygon>
                <polygon points="15,5 	20,0 	20,20 	15,15" fill="{{ $odontogram[44] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="R"
                    opacity="1"></polygon>
                <polygon points="0,0 	5,5 	5,15 	0,20" fill="{{ $odontogram[44] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="L" opacity="1">
                </polygon>
                <text x="6" y="30" stroke="navy" fill="navy" stroke-width="0.1"
                    style="font-size: 6pt;font-weight:normal">44</text>
            </g>
            <g id="P43" transform="translate(125,120)">
                <polygon points="5,5 	15,5 	15,15 	5,15" fill="{{ $odontogram[43] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="C"
                    opacity="1"></polygon>
                <polygon points="0,0 	20,0 	15,5 	5,5" fill="{{ $odontogram[43] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="T" opacity="1">
                </polygon>
                <polygon points="5,15 	15,15 	20,20 	0,20" fill="{{ $odontogram[43] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="B"
                    opacity="1"></polygon>
                <polygon points="15,5 	20,0 	20,20 	15,15" fill="{{ $odontogram[43] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="R"
                    opacity="1"></polygon>
                <polygon points="0,0 	5,5 	5,15 	0,20" fill="{{ $odontogram[43] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="L" opacity="1">
                </polygon>
                <text x="6" y="30" stroke="navy" fill="navy" stroke-width="0.1"
                    style="font-size: 6pt;font-weight:normal">43</text>
            </g>
            <g id="P42" transform="translate(150,120)">
                <polygon points="5,5 	15,5 	15,15 	5,15" fill="{{ $odontogram[42] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="C"
                    opacity="1"></polygon>
                <polygon points="0,0 	20,0 	15,5 	5,5" fill="{{ $odontogram[42] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="T" opacity="1">
                </polygon>
                <polygon points="5,15 	15,15 	20,20 	0,20" fill="{{ $odontogram[42] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="B"
                    opacity="1"></polygon>
                <polygon points="15,5 	20,0 	20,20 	15,15" fill="{{ $odontogram[42] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="R"
                    opacity="1"></polygon>
                <polygon points="0,0 	5,5 	5,15 	0,20" fill="{{ $odontogram[42] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="L" opacity="1">
                </polygon>
                <text x="6" y="30" stroke="navy" fill="navy" stroke-width="0.1"
                    style="font-size: 6pt;font-weight:normal">42</text>
            </g>
            <g id="P41" transform="translate(175,120)">
                <polygon points="5,5 	15,5 	15,15 	5,15" fill="{{ $odontogram[41] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="C"
                    opacity="1"></polygon>
                <polygon points="0,0 	20,0 	15,5 	5,5" fill="{{ $odontogram[41] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="T" opacity="1">
                </polygon>
                <polygon points="5,15 	15,15 	20,20 	0,20" fill="{{ $odontogram[41] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="B"
                    opacity="1"></polygon>
                <polygon points="15,5 	20,0 	20,20 	15,15" fill="{{ $odontogram[41] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="R"
                    opacity="1"></polygon>
                <polygon points="0,0 	5,5 	5,15 	0,20" fill="{{ $odontogram[41] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="L" opacity="1">
                </polygon>
                <text x="6" y="30" stroke="navy" fill="navy" stroke-width="0.1"
                    style="font-size: 6pt;font-weight:normal">41</text>
            </g>

            <!-- Row pertama baris kedua -->

            <g id="P21" transform="translate(210,0)">
                <polygon points="5,5 	15,5 	15,15 	5,15" fill="{{ $odontogram[21] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="C"
                    opacity="1"></polygon>
                <polygon points="0,0 	20,0 	15,5 	5,5" fill="{{ $odontogram[21] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="T" opacity="1">
                </polygon>
                <polygon points="5,15 	15,15 	20,20 	0,20" fill="{{ $odontogram[21] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="B"
                    opacity="1"></polygon>
                <polygon points="15,5 	20,0 	20,20 	15,15" fill="{{ $odontogram[21] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="R"
                    opacity="1"></polygon>
                <polygon points="0,0 	5,5 	5,15 	0,20" fill="{{ $odontogram[21] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="L" opacity="1">
                </polygon>
                <text x="6" y="30" stroke="navy" fill="navy" stroke-width="0.1"
                    style="font-size: 6pt;font-weight:normal">21</text>
            </g>
            <g id="P22" transform="translate(235,0)">
                <polygon points="5,5 	15,5 	15,15 	5,15" fill="{{ $odontogram[22] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="C"
                    opacity="1"></polygon>
                <polygon points="0,0 	20,0 	15,5 	5,5" fill="{{ $odontogram[22] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="T" opacity="1">
                </polygon>
                <polygon points="5,15 	15,15 	20,20 	0,20" fill="{{ $odontogram[22] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="B"
                    opacity="1"></polygon>
                <polygon points="15,5 	20,0 	20,20 	15,15" fill="{{ $odontogram[22] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="R"
                    opacity="1"></polygon>
                <polygon points="0,0 	5,5 	5,15 	0,20" fill="{{ $odontogram[22] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="L" opacity="1">
                </polygon>
                <text x="6" y="30" stroke="navy" fill="navy" stroke-width="0.1"
                    style="font-size: 6pt;font-weight:normal">22</text>
            </g>
            <g id="P23" transform="translate(260,0)">
                <polygon points="5,5 	15,5 	15,15 	5,15" fill="{{ $odontogram[23] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="C"
                    opacity="1"></polygon>
                <polygon points="0,0 	20,0 	15,5 	5,5" fill="{{ $odontogram[23] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="T" opacity="1">
                </polygon>
                <polygon points="5,15 	15,15 	20,20 	0,20" fill="{{ $odontogram[23] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="B"
                    opacity="1"></polygon>
                <polygon points="15,5 	20,0 	20,20 	15,15" fill="{{ $odontogram[23] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="R"
                    opacity="1"></polygon>
                <polygon points="0,0 	5,5 	5,15 	0,20" fill="{{ $odontogram[23] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="L" opacity="1">
                </polygon>
                <text x="6" y="30" stroke="navy" fill="navy" stroke-width="0.1"
                    style="font-size: 6pt;font-weight:normal">23</text>
            </g>
            <g id="P24" transform="translate(285,0)">
                <polygon points="5,5 	15,5 	15,15 	5,15" fill="{{ $odontogram[24] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="C"
                    opacity="1"></polygon>
                <polygon points="0,0 	20,0 	15,5 	5,5" fill="{{ $odontogram[24] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="T" opacity="1">
                </polygon>
                <polygon points="5,15 	15,15 	20,20 	0,20" fill="{{ $odontogram[24] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="B"
                    opacity="1"></polygon>
                <polygon points="15,5 	20,0 	20,20 	15,15" fill="{{ $odontogram[24] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="R"
                    opacity="1"></polygon>
                <polygon points="0,0 	5,5 	5,15 	0,20" fill="{{ $odontogram[24] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="L" opacity="1">
                </polygon>
                <text x="6" y="30" stroke="navy" fill="navy" stroke-width="0.1"
                    style="font-size: 6pt;font-weight:normal">24</text>
            </g>
            <g id="P25" transform="translate(310,0)">
                <polygon points="5,5 	15,5 	15,15 	5,15" fill="{{ $odontogram[25] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="C"
                    opacity="1"></polygon>
                <polygon points="0,0 	20,0 	15,5 	5,5" fill="{{ $odontogram[25] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="T" opacity="1">
                </polygon>
                <polygon points="5,15 	15,15 	20,20 	0,20" fill="{{ $odontogram[25] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="B"
                    opacity="1"></polygon>
                <polygon points="15,5 	20,0 	20,20 	15,15" fill="{{ $odontogram[25] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="R"
                    opacity="1"></polygon>
                <polygon points="0,0 	5,5 	5,15 	0,20" fill="{{ $odontogram[25] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="L" opacity="1">
                </polygon>
                <text x="6" y="30" stroke="navy" fill="navy" stroke-width="0.1"
                    style="font-size: 6pt;font-weight:normal">25</text>
            </g>
            <g id="P26" transform="translate(335,0)">
                <polygon points="5,5 	15,5 	15,15 	5,15" fill="{{ $odontogram[26] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="C"
                    opacity="1"></polygon>
                <polygon points="0,0 	20,0 	15,5 	5,5" fill="{{ $odontogram[26] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="T" opacity="1">
                </polygon>
                <polygon points="5,15 	15,15 	20,20 	0,20" fill="{{ $odontogram[26] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="B"
                    opacity="1"></polygon>
                <polygon points="15,5 	20,0 	20,20 	15,15" fill="{{ $odontogram[26] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="R"
                    opacity="1"></polygon>
                <polygon points="0,0 	5,5 	5,15 	0,20" fill="{{ $odontogram[26] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="L" opacity="1">
                </polygon>
                <text x="6" y="30" stroke="navy" fill="navy" stroke-width="0.1"
                    style="font-size: 6pt;font-weight:normal">26</text>
            </g>
            <g id="P27" transform="translate(360,0)">
                <polygon points="5,5 	15,5 	15,15 	5,15" fill="{{ $odontogram[27] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="C"
                    opacity="1"></polygon>
                <polygon points="0,0 	20,0 	15,5 	5,5" fill="{{ $odontogram[27] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="T" opacity="1">
                </polygon>
                <polygon points="5,15 	15,15 	20,20 	0,20" fill="{{ $odontogram[27] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="B"
                    opacity="1"></polygon>
                <polygon points="15,5 	20,0 	20,20 	15,15" fill="{{ $odontogram[27] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="R"
                    opacity="1"></polygon>
                <polygon points="0,0 	5,5 	5,15 	0,20" fill="{{ $odontogram[27] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="L" opacity="1">
                </polygon>
                <text x="6" y="30" stroke="navy" fill="navy" stroke-width="0.1"
                    style="font-size: 6pt;font-weight:normal">27</text>
            </g>
            <g id="P28" transform="translate(385,0)">
                <polygon points="5,5 	15,5 	15,15 	5,15" fill="{{ $odontogram[28] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="C"
                    opacity="1"></polygon>
                <polygon points="0,0 	20,0 	15,5 	5,5" fill="{{ $odontogram[28] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="T" opacity="1">
                </polygon>
                <polygon points="5,15 	15,15 	20,20 	0,20" fill="{{ $odontogram[28] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="B"
                    opacity="1"></polygon>
                <polygon points="15,5 	20,0 	20,20 	15,15" fill="{{ $odontogram[28] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="R"
                    opacity="1"></polygon>
                <polygon points="0,0 	5,5 	5,15 	0,20" fill="{{ $odontogram[28] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="L" opacity="1">
                </polygon>
                <text x="6" y="30" stroke="navy" fill="navy" stroke-width="0.1"
                    style="font-size: 6pt;font-weight:normal">28</text>
            </g>

            <!-- Deret kedua baris ke kedua -->

            <g id="P61" transform="translate(210,40)">
                <polygon points="5,5 	15,5 	15,15 	5,15" fill="{{ $odontogram[61] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="C"
                    opacity="1"></polygon>
                <polygon points="0,0 	20,0 	15,5 	5,5" fill="{{ $odontogram[61] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="T" opacity="1">
                </polygon>
                <polygon points="5,15 	15,15 	20,20 	0,20" fill="{{ $odontogram[61] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="B"
                    opacity="1"></polygon>
                <polygon points="15,5 	20,0 	20,20 	15,15" fill="{{ $odontogram[61] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="R"
                    opacity="1"></polygon>
                <polygon points="0,0 	5,5 	5,15 	0,20" fill="{{ $odontogram[61] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="L" opacity="1">
                </polygon>
                <text x="6" y="30" stroke="navy" fill="navy" stroke-width="0.1"
                    style="font-size: 6pt;font-weight:normal">61</text>
            </g>
            <g id="P62" transform="translate(235,40)">
                <polygon points="5,5 	15,5 	15,15 	5,15" fill="{{ $odontogram[62] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="C"
                    opacity="1"></polygon>
                <polygon points="0,0 	20,0 	15,5 	5,5" fill="{{ $odontogram[62] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="T" opacity="1">
                </polygon>
                <polygon points="5,15 	15,15 	20,20 	0,20" fill="{{ $odontogram[62] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="B"
                    opacity="1"></polygon>
                <polygon points="15,5 	20,0 	20,20 	15,15" fill="{{ $odontogram[62] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="R"
                    opacity="1"></polygon>
                <polygon points="0,0 	5,5 	5,15 	0,20" fill="{{ $odontogram[62] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="L" opacity="1">
                </polygon>
                <text x="6" y="30" stroke="navy" fill="navy" stroke-width="0.1"
                    style="font-size: 6pt;font-weight:normal">62</text>
            </g>
            <g id="P63" transform="translate(260,40)">
                <polygon points="5,5 	15,5 	15,15 	5,15" fill="{{ $odontogram[63] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="C"
                    opacity="1"></polygon>
                <polygon points="0,0 	20,0 	15,5 	5,5" fill="{{ $odontogram[63] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="T" opacity="1">
                </polygon>
                <polygon points="5,15 	15,15 	20,20 	0,20" fill="{{ $odontogram[63] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="B"
                    opacity="1"></polygon>
                <polygon points="15,5 	20,0 	20,20 	15,15" fill="{{ $odontogram[63] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="R"
                    opacity="1"></polygon>
                <polygon points="0,0 	5,5 	5,15 	0,20" fill="{{ $odontogram[63] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="L" opacity="1">
                </polygon>
                <text x="6" y="30" stroke="navy" fill="navy" stroke-width="0.1"
                    style="font-size: 6pt;font-weight:normal">63</text>
            </g>
            <g id="P64" transform="translate(285,40)">
                <polygon points="5,5 	15,5 	15,15 	5,15" fill="{{ $odontogram[64] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="C"
                    opacity="1"></polygon>
                <polygon points="0,0 	20,0 	15,5 	5,5" fill="{{ $odontogram[64] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="T" opacity="1">
                </polygon>
                <polygon points="5,15 	15,15 	20,20 	0,20" fill="{{ $odontogram[64] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="B"
                    opacity="1"></polygon>
                <polygon points="15,5 	20,0 	20,20 	15,15" fill="{{ $odontogram[64] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="R"
                    opacity="1"></polygon>
                <polygon points="0,0 	5,5 	5,15 	0,20" fill="{{ $odontogram[64] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="L" opacity="1">
                </polygon>
                <text x="6" y="30" stroke="navy" fill="navy" stroke-width="0.1"
                    style="font-size: 6pt;font-weight:normal">64</text>
            </g>
            <g id="P65" transform="translate(310,40)">
                <polygon points="5,5 	15,5 	15,15 	5,15" fill="{{ $odontogram[65] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="C"
                    opacity="1"></polygon>
                <polygon points="0,0 	20,0 	15,5 	5,5" fill="{{ $odontogram[65] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="T" opacity="1">
                </polygon>
                <polygon points="5,15 	15,15 	20,20 	0,20" fill="{{ $odontogram[65] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="B"
                    opacity="1"></polygon>
                <polygon points="15,5 	20,0 	20,20 	15,15" fill="{{ $odontogram[65] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="R"
                    opacity="1"></polygon>
                <polygon points="0,0 	5,5 	5,15 	0,20" fill="{{ $odontogram[65] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="L" opacity="1">
                </polygon>
                <text x="6" y="30" stroke="navy" fill="navy" stroke-width="0.1"
                    style="font-size: 6pt;font-weight:normal">65</text>
            </g>

            <!-- Deret ketiga baris kedua -->

            <g id="P71" transform="translate(210,80)">
                <polygon points="5,5 	15,5 	15,15 	5,15" fill="{{ $odontogram[71] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="C"
                    opacity="1"></polygon>
                <polygon points="0,0 	20,0 	15,5 	5,5" fill="{{ $odontogram[71] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="T" opacity="1">
                </polygon>
                <polygon points="5,15 	15,15 	20,20 	0,20" fill="{{ $odontogram[71] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="B"
                    opacity="1"></polygon>
                <polygon points="15,5 	20,0 	20,20 	15,15" fill="{{ $odontogram[71] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="R"
                    opacity="1"></polygon>
                <polygon points="0,0 	5,5 	5,15 	0,20" fill="{{ $odontogram[71] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="L" opacity="1">
                </polygon>
                <text x="6" y="30" stroke="navy" fill="navy" stroke-width="0.1"
                    style="font-size: 6pt;font-weight:normal">71</text>
            </g>
            <g id="P72" transform="translate(235,80)">
                <polygon points="5,5 	15,5 	15,15 	5,15" fill="{{ $odontogram[72] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="C"
                    opacity="1"></polygon>
                <polygon points="0,0 	20,0 	15,5 	5,5" fill="{{ $odontogram[72] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="T" opacity="1">
                </polygon>
                <polygon points="5,15 	15,15 	20,20 	0,20" fill="{{ $odontogram[72] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="B"
                    opacity="1"></polygon>
                <polygon points="15,5 	20,0 	20,20 	15,15" fill="{{ $odontogram[72] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="R"
                    opacity="1"></polygon>
                <polygon points="0,0 	5,5 	5,15 	0,20" fill="{{ $odontogram[72] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="L" opacity="1">
                </polygon>
                <text x="6" y="30" stroke="navy" fill="navy" stroke-width="0.1"
                    style="font-size: 6pt;font-weight:normal">72</text>
            </g>
            <g id="P73" transform="translate(260,80)">
                <polygon points="5,5 	15,5 	15,15 	5,15" fill="{{ $odontogram[73] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="C"
                    opacity="1"></polygon>
                <polygon points="0,0 	20,0 	15,5 	5,5" fill="{{ $odontogram[73] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="T" opacity="1">
                </polygon>
                <polygon points="5,15 	15,15 	20,20 	0,20" fill="{{ $odontogram[73] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="B"
                    opacity="1"></polygon>
                <polygon points="15,5 	20,0 	20,20 	15,15" fill="{{ $odontogram[73] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="R"
                    opacity="1"></polygon>
                <polygon points="0,0 	5,5 	5,15 	0,20" fill="{{ $odontogram[73] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="L" opacity="1">
                </polygon>
                <text x="6" y="30" stroke="navy" fill="navy" stroke-width="0.1"
                    style="font-size: 6pt;font-weight:normal">73</text>
            </g>
            <g id="P74" transform="translate(285,80)">
                <polygon points="5,5 	15,5 	15,15 	5,15" fill="{{ $odontogram[74] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="C"
                    opacity="1"></polygon>
                <polygon points="0,0 	20,0 	15,5 	5,5" fill="{{ $odontogram[74] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="T" opacity="1">
                </polygon>
                <polygon points="5,15 	15,15 	20,20 	0,20" fill="{{ $odontogram[74] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="B"
                    opacity="1"></polygon>
                <polygon points="15,5 	20,0 	20,20 	15,15" fill="{{ $odontogram[74] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="R"
                    opacity="1"></polygon>
                <polygon points="0,0 	5,5 	5,15 	0,20" fill="{{ $odontogram[74] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="L" opacity="1">
                </polygon>
                <text x="6" y="30" stroke="navy" fill="navy" stroke-width="0.1"
                    style="font-size: 6pt;font-weight:normal">74</text>
            </g>
            <g id="P75" transform="translate(310,80)">
                <polygon points="5,5 	15,5 	15,15 	5,15" fill="{{ $odontogram[75] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="C"
                    opacity="1"></polygon>
                <polygon points="0,0 	20,0 	15,5 	5,5" fill="{{ $odontogram[75] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="T" opacity="1">
                </polygon>
                <polygon points="5,15 	15,15 	20,20 	0,20" fill="{{ $odontogram[75] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="B"
                    opacity="1"></polygon>
                <polygon points="15,5 	20,0 	20,20 	15,15" fill="{{ $odontogram[75] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="R"
                    opacity="1"></polygon>
                <polygon points="0,0 	5,5 	5,15 	0,20" fill="{{ $odontogram[75] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="L" opacity="1">
                </polygon>
                <text x="6" y="30" stroke="navy" fill="navy" stroke-width="0.1"
                    style="font-size: 6pt;font-weight:normal">75</text>
            </g>

            <!-- Deret ke empat baris kedua -->

            <g id="P31" transform="translate(210,120)">
                <polygon points="5,5 	15,5 	15,15 	5,15" fill="{{ $odontogram[31] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="C"
                    opacity="1"></polygon>
                <polygon points="0,0 	20,0 	15,5 	5,5" fill="{{ $odontogram[31] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="T" opacity="1">
                </polygon>
                <polygon points="5,15 	15,15 	20,20 	0,20" fill="{{ $odontogram[31] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="B"
                    opacity="1"></polygon>
                <polygon points="15,5 	20,0 	20,20 	15,15" fill="{{ $odontogram[31] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="R"
                    opacity="1"></polygon>
                <polygon points="0,0 	5,5 	5,15 	0,20" fill="{{ $odontogram[31] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="L" opacity="1">
                </polygon>
                <text x="6" y="30" stroke="navy" fill="navy" stroke-width="0.1"
                    style="font-size: 6pt;font-weight:normal">31</text>
            </g>
            <g id="P32" transform="translate(235,120)">
                <polygon points="5,5 	15,5 	15,15 	5,15" fill="{{ $odontogram[32] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="C"
                    opacity="1"></polygon>
                <polygon points="0,0 	20,0 	15,5 	5,5" fill="{{ $odontogram[32] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="T" opacity="1">
                </polygon>
                <polygon points="5,15 	15,15 	20,20 	0,20" fill="{{ $odontogram[32] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="B"
                    opacity="1"></polygon>
                <polygon points="15,5 	20,0 	20,20 	15,15" fill="{{ $odontogram[32] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="R"
                    opacity="1"></polygon>
                <polygon points="0,0 	5,5 	5,15 	0,20" fill="{{ $odontogram[32] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="L" opacity="1">
                </polygon>
                <text x="6" y="30" stroke="navy" fill="navy" stroke-width="0.1"
                    style="font-size: 6pt;font-weight:normal">32</text>
            </g>
            <g id="P33" transform="translate(260,120)">
                <polygon points="5,5 	15,5 	15,15 	5,15" fill="{{ $odontogram[33] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="C"
                    opacity="1"></polygon>
                <polygon points="0,0 	20,0 	15,5 	5,5" fill="{{ $odontogram[33] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="T" opacity="1">
                </polygon>
                <polygon points="5,15 	15,15 	20,20 	0,20" fill="{{ $odontogram[33] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="B"
                    opacity="1"></polygon>
                <polygon points="15,5 	20,0 	20,20 	15,15" fill="{{ $odontogram[33] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="R"
                    opacity="1"></polygon>
                <polygon points="0,0 	5,5 	5,15 	0,20" fill="{{ $odontogram[33] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="L" opacity="1">
                </polygon>
                <text x="6" y="30" stroke="navy" fill="navy" stroke-width="0.1"
                    style="font-size: 6pt;font-weight:normal">33</text>
            </g>
            <g id="P34" transform="translate(285,120)">
                <polygon points="5,5 	15,5 	15,15 	5,15" fill="{{ $odontogram[34] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="C" opacity="1">
                </polygon>
                <polygon points="0,0 	20,0 	15,5 	5,5" fill="{{ $odontogram[34] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="T" opacity="1">
                </polygon>
                <polygon points="5,15 	15,15 	20,20 	0,20" fill="{{ $odontogram[34] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="B"
                    opacity="1"></polygon>
                <polygon points="15,5 	20,0 	20,20 	15,15" fill="{{ $odontogram[34] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="R"
                    opacity="1"></polygon>
                <polygon points="0,0 	5,5 	5,15 	0,20" fill="{{ $odontogram[34] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="L" opacity="1">
                </polygon>
                <text x="6" y="30" stroke="navy" fill="navy" stroke-width="0.1"
                    style="font-size: 6pt;font-weight:normal">34</text>
            </g>
            <g id="P35" transform="translate(310,120)">
                <polygon points="5,5 	15,5 	15,15 	5,15" fill="{{ $odontogram[35] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="C"
                    opacity="1"></polygon>
                <polygon points="0,0 	20,0 	15,5 	5,5" fill="{{ $odontogram[35] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="T" opacity="1">
                </polygon>
                <polygon points="5,15 	15,15 	20,20 	0,20" fill="{{ $odontogram[35] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="B"
                    opacity="1"></polygon>
                <polygon points="15,5 	20,0 	20,20 	15,15" fill="{{ $odontogram[35] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="R"
                    opacity="1"></polygon>
                <polygon points="0,0 	5,5 	5,15 	0,20" fill="{{ $odontogram[35] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="L" opacity="1">
                </polygon>
                <text x="6" y="30" stroke="navy" fill="navy" stroke-width="0.1"
                    style="font-size: 6pt;font-weight:normal">35</text>
            </g>
            <g id="P36" transform="translate(335,120)">
                <polygon points="5,5 	15,5 	15,15 	5,15" fill="{{ $odontogram[36] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="C"
                    opacity="1"></polygon>
                <polygon points="0,0 	20,0 	15,5 	5,5" fill="{{ $odontogram[36] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="T" opacity="1">
                </polygon>
                <polygon points="5,15 	15,15 	20,20 	0,20" fill="{{ $odontogram[36] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="B"
                    opacity="1"></polygon>
                <polygon points="15,5 	20,0 	20,20 	15,15" fill="{{ $odontogram[36] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="R"
                    opacity="1"></polygon>
                <polygon points="0,0 	5,5 	5,15 	0,20" fill="{{ $odontogram[36] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="L" opacity="1">
                </polygon>
                <text x="6" y="30" stroke="navy" fill="navy" stroke-width="0.1"
                    style="font-size: 6pt;font-weight:normal">36</text>
            </g>
            <g id="P37" transform="translate(360,120)">
                <polygon points="5,5 	15,5 	15,15 	5,15" fill="{{ $odontogram[37] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="C"
                    opacity="1"></polygon>
                <polygon points="0,0 	20,0 	15,5 	5,5" fill="{{ $odontogram[37] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="T" opacity="1">
                </polygon>
                <polygon points="5,15 	15,15 	20,20 	0,20" fill="{{ $odontogram[37] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="B"
                    opacity="1"></polygon>
                <polygon points="15,5 	20,0 	20,20 	15,15" fill="{{ $odontogram[37] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="R"
                    opacity="1"></polygon>
                <polygon points="0,0 	5,5 	5,15 	0,20" fill="{{ $odontogram[37] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="L" opacity="1">
                </polygon>
                <text x="6" y="30" stroke="navy" fill="navy" stroke-width="0.1"
                    style="font-size: 6pt;font-weight:normal">37</text>
            </g>
            <g id="P38" transform="translate(385,120)">
                <polygon points="5,5 	15,5 	15,15 	5,15" fill="{{ $odontogram[38] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="C"
                    opacity="1"></polygon>
                <polygon points="0,0 	20,0 	15,5 	5,5" fill="{{ $odontogram[38] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="T" opacity="1">
                </polygon>
                <polygon points="5,15 	15,15 	20,20 	0,20" fill="{{ $odontogram[38] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="B"
                    opacity="1"></polygon>
                <polygon points="15,5 	20,0 	20,20 	15,15" fill="{{ $odontogram[38] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="R"
                    opacity="1"></polygon>
                <polygon points="0,0 	5,5 	5,15 	0,20" fill="{{ $odontogram[38] ?? 'white' }}" stroke="navy" stroke-width="0.5" id="L" opacity="1">
                </polygon>
                <text x="6" y="30" stroke="navy" fill="navy" stroke-width="0.1"
                    style="font-size: 6pt;font-weight:normal">38</text>
            </g>
        </g>

        {{-- keterangan warna 1 --}}

        <g transform="translate(0,250)">
            <polygon points="5,5 	15,5 	15,15 	5,15" fill="white" stroke="navy" stroke-width="0.5" id="C" opacity="1">
            </polygon>
            <polygon points="0,0 	20,0 	15,5 	5,5" fill="white" stroke="navy" stroke-width="0.5" id="T" opacity="1">
            </polygon>
            <polygon points="5,15 	15,15 	20,20 	0,20" fill="white" stroke="navy" stroke-width="0.5" id="B" opacity="1">
            </polygon>
            <polygon points="15,5 	20,0 	20,20 	15,15" fill="white" stroke="navy" stroke-width="0.5" id="R" opacity="1">
            </polygon>
            <polygon points="0,0 	5,5 	5,15 	0,20" fill="white" stroke="navy" stroke-width="0.5" id="L" opacity="1">
            </polygon>
            <text x="25" y="15" stroke="navy" fill="navy" stroke-width="0.1"
                style="font-size: 9pt;font-weight:normal">Sehat</text>
        </g>
        <g transform="translate(125,250)">
            <polygon points="5,5 	15,5 	15,15 	5,15" fill="red" stroke="navy" stroke-width="0.5" id="C" opacity="1">
            </polygon>
            <polygon points="0,0 	20,0 	15,5 	5,5" fill="red" stroke="navy" stroke-width="0.5" id="T" opacity="1">
            </polygon>
            <polygon points="5,15 	15,15 	20,20 	0,20" fill="red" stroke="navy" stroke-width="0.5" id="B" opacity="1">
            </polygon>
            <polygon points="15,5 	20,0 	20,20 	15,15" fill="red" stroke="navy" stroke-width="0.5" id="R" opacity="1">
            </polygon>
            <polygon points="0,0 	5,5 	5,15 	0,20" fill="red" stroke="navy" stroke-width="0.5" id="L" opacity="1">
            </polygon>
            <text x="25" y="15" stroke="navy" fill="navy" stroke-width="0.1"
                style="font-size: 9pt;font-weight:normal">Lubang</text>
        </g>
        <g transform="translate(250,250)">
            <polygon points="5,5 	15,5 	15,15 	5,15" fill="black" stroke="navy" stroke-width="0.5" id="C" opacity="1">
            </polygon>
            <polygon points="0,0 	20,0 	15,5 	5,5" fill="black" stroke="navy" stroke-width="0.5" id="T" opacity="1">
            </polygon>
            <polygon points="5,15 	15,15 	20,20 	0,20" fill="black" stroke="navy" stroke-width="0.5" id="B"
                opacity="1"></polygon>
            <polygon points="15,5 	20,0 	20,20 	15,15" fill="black" stroke="navy" stroke-width="0.5" id="R"
                opacity="1"></polygon>
            <polygon points="0,0 	5,5 	5,15 	0,20" fill="black" stroke="navy" stroke-width="0.5" id="L" opacity="1">
            </polygon>
            <text x="25" y="15" stroke="navy" fill="navy" stroke-width="0.1"
                style="font-size: 9pt;font-weight:normal">Hilang</text>
        </g>
        <g transform="translate(375,250)">
            <polygon points="5,5 	15,5 	15,15 	5,15" fill="green" stroke="navy" stroke-width="0.5" id="C" opacity="1">
            </polygon>
            <polygon points="0,0 	20,0 	15,5 	5,5" fill="green" stroke="navy" stroke-width="0.5" id="T" opacity="1">
            </polygon>
            <polygon points="5,15 	15,15 	20,20 	0,20" fill="green" stroke="navy" stroke-width="0.5" id="B" opacity="1">
            </polygon>
            <polygon points="15,5 	20,0 	20,20 	15,15" fill="green" stroke="navy" stroke-width="0.5" id="R" opacity="1">
            </polygon>
            <polygon points="0,0 	5,5 	5,15 	0,20" fill="green" stroke="navy" stroke-width="0.5" id="L" opacity="1">
            </polygon>
            <text x="25" y="15" stroke="navy" fill="navy" stroke-width="0.1"
                style="font-size: 9pt;font-weight:normal">Anak</text>
        </g>
        <g transform="translate(500,250)">
            <polygon points="5,5 	15,5 	15,15 	5,15" fill="blue" stroke="navy" stroke-width="0.5" id="C" opacity="1">
            </polygon>
            <polygon points="0,0 	20,0 	15,5 	5,5" fill="blue" stroke="navy" stroke-width="0.5" id="T" opacity="1">
            </polygon>
            <polygon points="5,15 	15,15 	20,20 	0,20" fill="blue" stroke="navy" stroke-width="0.5" id="B" opacity="1">
            </polygon>
            <polygon points="15,5 	20,0 	20,20 	15,15" fill="blue" stroke="navy" stroke-width="0.5" id="R" opacity="1">
            </polygon>
            <polygon points="0,0 	5,5 	5,15 	0,20" fill="blue" stroke="navy" stroke-width="0.5" id="L" opacity="1">
            </polygon>
            <text x="25" y="15" stroke="navy" fill="navy" stroke-width="0.1"
                style="font-size: 9pt;font-weight:normal">Dewasa</text>
        </g>

        {{-- keterangan warna 1 --}}

        <g transform="translate(0,300)">
            <polygon points="5,5 	15,5 	15,15 	5,15" fill="pink" stroke="navy" stroke-width="0.5" id="C" opacity="1">
            </polygon>
            <polygon points="0,0 	20,0 	15,5 	5,5" fill="pink" stroke="navy" stroke-width="0.5" id="T" opacity="1">
            </polygon>
            <polygon points="5,15 	15,15 	20,20 	0,20" fill="pink" stroke="navy" stroke-width="0.5" id="B" opacity="1">
            </polygon>
            <polygon points="15,5 	20,0 	20,20 	15,15" fill="pink" stroke="navy" stroke-width="0.5" id="R" opacity="1">
            </polygon>
            <polygon points="0,0 	5,5 	5,15 	0,20" fill="pink" stroke="navy" stroke-width="0.5" id="L" opacity="1">
            </polygon>
            <text x="25" y="15" stroke="navy" fill="navy" stroke-width="0.1"
                style="font-size: 9pt;font-weight:normal">Jaringan tdk sehat</text>
        </g>
        <g transform="translate(125,300)">
            <polygon points="5,5 	15,5 	15,15 	5,15" fill="gray" stroke="navy" stroke-width="0.5" id="C" opacity="1">
            </polygon>
            <polygon points="0,0 	20,0 	15,5 	5,5" fill="gray" stroke="navy" stroke-width="0.5" id="T" opacity="1">
            </polygon>
            <polygon points="5,15 	15,15 	20,20 	0,20" fill="gray" stroke="navy" stroke-width="0.5" id="B" opacity="1">
            </polygon>
            <polygon points="15,5 	20,0 	20,20 	15,15" fill="gray" stroke="navy" stroke-width="0.5" id="R" opacity="1">
            </polygon>
            <polygon points="0,0 	5,5 	5,15 	0,20" fill="gray" stroke="navy" stroke-width="0.5" id="L" opacity="1">
            </polygon>
            <text x="25" y="15" stroke="navy" fill="navy" stroke-width="0.1"
                style="font-size: 9pt;font-weight:normal">Akar gigi</text>
        </g>
        <g transform="translate(250,300)">
            <polygon points="5,5 	15,5 	15,15 	5,15" fill="yellow" stroke="navy" stroke-width="0.5" id="C" opacity="1">
            </polygon>
            <polygon points="0,0 	20,0 	15,5 	5,5" fill="yellow" stroke="navy" stroke-width="0.5" id="T" opacity="1">
            </polygon>
            <polygon points="5,15 	15,15 	20,20 	0,20" fill="yellow" stroke="navy" stroke-width="0.5" id="B"
                opacity="1"></polygon>
            <polygon points="15,5 	20,0 	20,20 	15,15" fill="yellow" stroke="navy" stroke-width="0.5" id="R"
                opacity="1"></polygon>
            <polygon points="0,0 	5,5 	5,15 	0,20" fill="yellow" stroke="navy" stroke-width="0.5" id="L" opacity="1">
            </polygon>
            <text x="25" y="15" stroke="navy" fill="navy" stroke-width="0.1"
                style="font-size: 9pt;font-weight:normal">Palsu</text>
        </g>
        <g transform="translate(375,300)">
            <polygon points="5,5 	15,5 	15,15 	5,15" fill="Wheat" stroke="navy" stroke-width="0.5" id="C" opacity="1">
            </polygon>
            <polygon points="0,0 	20,0 	15,5 	5,5" fill="Wheat" stroke="navy" stroke-width="0.5" id="T" opacity="1">
            </polygon>
            <polygon points="5,15 	15,15 	20,20 	0,20" fill="Wheat" stroke="navy" stroke-width="0.5" id="B" opacity="1">
            </polygon>
            <polygon points="15,5 	20,0 	20,20 	15,15" fill="Wheat" stroke="navy" stroke-width="0.5" id="R" opacity="1">
            </polygon>
            <polygon points="0,0 	5,5 	5,15 	0,20" fill="Wheat" stroke="navy" stroke-width="0.5" id="L" opacity="1">
            </polygon>
            <text x="25" y="15" stroke="navy" fill="navy" stroke-width="0.1"
                style="font-size: 9pt;font-weight:normal">Tidak tumbuh</text>
        </g>
        <g transform="translate(500,300)">
            <polygon points="5,5 	15,5 	15,15 	5,15" fill="Chocolate" stroke="navy" stroke-width="0.5" id="C" opacity="1">
            </polygon>
            <polygon points="0,0 	20,0 	15,5 	5,5" fill="Chocolate" stroke="navy" stroke-width="0.5" id="T" opacity="1">
            </polygon>
            <polygon points="5,15 	15,15 	20,20 	0,20" fill="Chocolate" stroke="navy" stroke-width="0.5" id="B" opacity="1">
            </polygon>
            <polygon points="15,5 	20,0 	20,20 	15,15" fill="Chocolate" stroke="navy" stroke-width="0.5" id="R" opacity="1">
            </polygon>
            <polygon points="0,0 	5,5 	5,15 	0,20" fill="Chocolate" stroke="navy" stroke-width="0.5" id="L" opacity="1">
            </polygon>
            <text x="25" y="15" stroke="navy" fill="navy" stroke-width="0.1"
                style="font-size: 9pt;font-weight:normal">Gusi tdk sehat</text>
        </g>
    </svg>
</div>