{{-- @if(Auth::user()->admin == 1)
<div class="form-group {{ $errors->has('user_id') ? 'has-error' : ''}}">
    <label for="user_id" class="control-label">{{ 'User Id' }}</label>
    <input class="form-control" name="user_id" type="number" id="user_id" value="{{ isset($riwayat->user_id) ? $riwayat->user_id : ''}}" >
    {!! $errors->first('user_id', '<p class="help-block">:message</p>') !!}
</div>    
@endif --}}
<div class="form-group {{ $errors->has('judul') ? 'has-error' : ''}}">
    <label for="judul" class="control-label">{{ 'Judul' }}</label>
    <input class="form-control" name="judul" type="text" id="judul" value="{{ isset($riwayat->judul) ? $riwayat->judul : ''}}" >
    {!! $errors->first('judul', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('deskripsi') ? 'has-error' : ''}}">
    <label for="deskripsi" class="control-label">{{ 'Deskripsi' }}</label>
    <textarea class="form-control" rows="5" name="deskripsi" type="textarea" id="deskripsi" >{{ isset($riwayat->deskripsi) ? $riwayat->deskripsi : ''}}</textarea>
    {!! $errors->first('deskripsi', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('tgl') ? 'has-error' : ''}}">
    <label for="tgl" class="control-label">{{ 'Tanggal Gejala' }}</label>
    <input class="form-control" name="tgl" type="date" id="tgl" value="{{ isset($riwayat->tgl) ? $riwayat->tgl : ''}}" >
    {!! $errors->first('tgl', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('tag') ? 'has-error' : ''}}">
    <label for="tag" class="control-label">{{ 'Tag' }}</label> <sup>(*optional)</sup>
    <input class="form-control" name="tag" type="text" id="tag" value="{{ isset($riwayat->tag) ? $riwayat->tag : ''}}" >
    {!! $errors->first('tag', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>
