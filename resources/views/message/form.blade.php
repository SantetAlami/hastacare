{{-- <div class="form-group {{ $errors->has('ticket_id') ? 'has-error' : ''}}"> --}}
    {{-- <label for="ticket_id" class="control-label">{{ 'Ticket Id' }}</label> --}}
    <input class="form-control" name="ticket_id" type="hidden" id="ticket_id" value="{{ isset($message->ticket_id) ? $message->ticket_id : (isset($ticket_id) ? $ticket_id : '')}}" >
    {{-- {!! $errors->first('ticket_id', '<p class="help-block">:message</p>') !!} --}}
{{-- </div> --}}
<div class="form-group {{ $errors->has('message') ? 'has-error' : ''}}">
    <label for="message" class="control-label">{{ 'Message' }}</label>
    <textarea class="form-control" rows="5" name="message" type="textarea" id="message" >{{ isset($message->message) ? $message->message : ''}}</textarea>
    {!! $errors->first('message', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>
