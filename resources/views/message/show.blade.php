@extends('layouts.app')

@section('content')
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">Message {{ $message->id }}</div>
            <div class="card-body">

                <a href="{{ url('/message') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                <a href="{{ url('/message/' . $message->id . '/edit') }}" title="Edit Message"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                <form method="POST" action="{{ url('message' . '/' . $message->id) }}" accept-charset="UTF-8" style="display:inline">
                    {{ method_field('DELETE') }}
                    {{ csrf_field() }}
                    <button type="submit" class="btn btn-danger btn-sm" title="Delete Message" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                </form>
                <br/>
                <br/>

                <div class="table-responsive">
                    <table class="table">
                        <tbody>
                            <tr>
                                <th>ID</th><td>{{ $message->id }}</td>
                            </tr>
                            <tr><th> User Id </th><td> {{ $message->user_id }} </td></tr><tr><th> Ticket Id </th><td> {{ $message->ticket_id }} </td></tr><tr><th> Message </th><td> {{ $message->message }} </td></tr>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
@endsection
