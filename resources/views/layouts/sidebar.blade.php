{{-- <div class="col-md-3">
    <div class="card">
        <div class="card-header">
            Sidebar
        </div>

        <div class="card-body">
            <ul class="nav">
                <li>
                    <a href="{{ url('/home') }}">
Dashboard
</a>
</li>
</ul>
<ul class="nav">
  <li>
    <a href="{{ url('/riwayat') }}">
      Riwayat Kesehatan
    </a>
  </li>
</ul>
<ul class="nav">
  <li>
    <a href="{{ url('/ticket') }}">
      Tiket Keluhan
    </a>
  </li>
</ul>
</div>
</div>
</div> --}}

<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
  <!-- Brand Logo -->
  <a href="{{ url('/') }}" class="brand-link">
    <i class="fa fa-user-circle" style="font-size: 32px; position: absolute; left: 20px"></i>
    {{-- <img src="{{ asset('dist/img/AdminLTELogo.png') }}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
      style="opacity: .8"> --}}
    <span class="brand-text font-weight-light" style="padding-left: 50px">{{ Auth::user()->name }}</span>
  </a>

  <!-- Sidebar -->
  <div class="sidebar">

    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <li class="nav-item">
          <a href="{{ route('home') }}" class="nav-link {{ Request::is('home') || Request::is('home') ? 'active' : '' }}">
            <i class="nav-icon fas fa-tachometer-alt"></i>
            <p>
              Dashboard
              {{-- <span class="right badge badge-danger">New</span> --}}
            </p>
          </a>
        </li>
        @if (Auth::user()->admin == 1)            
        <li class="nav-item">
          <a href="{{ url('monitor') }}" class="nav-link {{ Request::is('monitor') || Request::is('monitor') ? 'active' : '' }}">
            <i class="nav-icon fa fa-bar-chart"></i>
            <p>
              Monitor Keaktifan
            </p>
          </a>
        </li>
        <li class="nav-item {{ Request::is('master/*') ? 'menu-open' : '' }}">
          <a href="#" class="nav-link {{ Request::is('master/*') ? 'active' : '' }}">
            <i class="nav-icon fa fa-database"></i>
            <p>
              Master
              <i class="right fas fa-angle-left"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="{{ url('master/user') }}" class="nav-link {{ Request::is('master/user') || Request::is('master/user/*') ? 'active' : '' }}">
                <i class="far fa-circle nav-icon"></i>
                <p>Data User</p>
              </a>
            </li>
          </ul>
        </li>
        @endif
        <li class="nav-item menu-open">
          <a href="#" class="nav-link active">
            <i class="nav-icon fas fa-th"></i>
            <p>
              Menu
              <i class="right fas fa-angle-left"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="{{ route('riwayat') }}" class="nav-link {{ Request::is('profil_kesehatan') || Request::is('profil_kesehatan/*') ? 'active' : '' }}">
                <i class="far fa-circle nav-icon"></i>
                <p>Profil Kesehatan</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{ url('ticket_kesehatan') }}" class="nav-link {{ Request::is('ticket_kesehatan') || Request::is('ticket_kesehatan/*') ? 'active' : '' }}">
                <i class="far fa-circle nav-icon"></i>
                <p>Ticket Kesehatan</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{ url('sarana_dan_pelayanan') }}" class="nav-link {{ Request::is('sarana_dan_pelayanan') || Request::is('sarana_dan_pelayanan/*') ? 'active' : '' }}">
                <i class="far fa-circle nav-icon"></i>
                <p>Saranan pelayanan</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{ url('ketersediaan_obat') }}" class="nav-link {{ Request::is('ketersediaan_obat') || Request::is('ketersediaan_obat/*') ? 'active' : '' }}">
                <i class="far fa-circle nav-icon"></i>
                <p>Ketersediaan Obat</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{ url('panic') }}" class="nav-link {{ Request::is('panic') || Request::is('panic/*') ? 'active' : '' }}">
                <i class="far fa-circle nav-icon"></i>
                <p>Panic Area</p>
              </a>
            </li>
          </ul>
        </li>
      </ul>
    </nav>
    <!-- /.sidebar-menu -->
  </div>
  <!-- /.sidebar -->
</aside>