<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
    <meta name="user-id" content="{{ Auth::user()->id ?? '0' }}">
    <meta name="admin" content="{{ Auth::user()->admin ?? '0' }}">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>HastaCare</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('plugins/font-awesome/css/font-awesome.min.css') }}">
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <title>HastaCare | Dashboard</title>

    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('plugins/fontawesome-free/css/all.min.css') }}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Tempusdominus Bootstrap 4 -->
    <link rel="stylesheet"
        href="{{ asset('plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css') }}">
    <!-- iCheck -->
    <link rel="stylesheet" href="{{ asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
    <!-- JQVMap -->
    <link rel="stylesheet" href="{{ asset('plugins/jqvmap/jqvmap.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('dist/css/adminlte.min.css') }}">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="{{ asset('plugins/overlayScrollbars/css/OverlayScrollbars.min.css') }}">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="{{ asset('plugins/daterangepicker/daterangepicker.css') }}">
    <!-- summernote -->
    <link rel="stylesheet" href="{{ asset('plugins/summernote/summernote-bs4.min.css') }}">

    <audio id="sos_audio" src="{{ asset('dist/sound/sos.mp3') }}"></audio>
    <style>
        .floating-button {
            position: fixed;
            bottom: 0;
            right: 0;
            z-index: 2;
        }
    </style>
    @yield('style')

</head>

<body class="hold-transition {{ Request::is('login') || Request::is('register') ? '' : 'sidebar-mini' }}">
    <div id="app" class="{{ Request::is('login') || Request::is('register') ? 'login-page' : 'wrapper' }}">
        <div id="pnc"
            style="top:0; width:100%; height:100%; position: fixed; background-color:rgba(255,0,0,0.6); z-index:1039; display:none">
            <form method="POST" action="{{ url('/panic') }}" accept-charset="UTF-8" class="form-horizontal"
                enctype="multipart/form-data">
                {{ csrf_field() }}

                <button type="submit">
                    <img style="background-color: transparent;
                        position: absolute;
                        top:50%;
                        left:50%;
                        max-width: 100%;
                        transform: translate(-50%,-50%);
                        " src="{{ asset('dist/img/panic.png') }}">
                </button>
            </form>
            <b style="background-color: white;
                    text-align:center;
                    position: absolute;
                    top:0%;
                    left:50%;
                    transform: translate(-50%,0%);
                    padding: 20px;
                    ">
                Tekan tombol hanya dalam keadaan darurat
            </b>
        </div>
        @if (!Request::is('login') && !Request::is('register'))
        @include('layouts.header')
        @include('layouts.sidebar')
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    @php
                    $url = explode('/',Request::path());
                    @endphp
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1 class="m-0">
                            @foreach($url as $k => $urlname)
                            @if ($k > 0)
                                |
                            @endif
                            {{ ucfirst(str_replace('_',' ', $urlname)) }}
                            @endforeach
                            </h1>
                        </div><!-- /.col -->
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item active"><a href="/home">Home</a></li>
                                @foreach($url as $k => $urlname)
                                <li class="breadcrumb-item">
                                    @if ($k == count($url) - 1)
                                    {{ ucfirst(str_replace('_',' ',$urlname)) }}
                                    @else
                                    <a href="{{ str_repeat('../',(($k-count($url)+1)*-1)) }}{{ $urlname }}">
                                        {{ ucfirst(str_replace('_',' ',$urlname)) }}
                                    </a>
                                    @endif
                                </li>
                                @endforeach
                            </ol>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

            <!-- Main content -->
            <div class="content">
                <div class="container-fluid">
                    @if (session('flash_message'))
                    <br>
                    <div class="callout callout-success">
                        <h4>Success!</h4>

                        <p>{{ session('flash_message') }}</p>
                    </div>
                    @endif
                    @if (session('error_message'))
                    <br>
                    <div class="callout callout-danger">
                        <h4>Error!</h4>

                        <p>{{ session('error_message') }}</p>
                    </div>
                    @endif
                    <div class="row">
                        @yield('content')
                    </div>
                    <!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        <!-- Main Footer -->
        <footer class="main-footer">
            <!-- To the right -->
            @if(Auth::user()->admin == 1)
            <div class="float-right" style="position: fixed;
            bottom: 0;
            right: 0;">
                <a href="{{ url('/panic') }}" class="btn btn-danger btn-lg">Panic Area</a>
            </div>
            @else
            <div class="float-right" style="position: fixed;
            bottom: 0;
            right: 0;">
                <a href="#" id="panic" class="btn btn-danger btn-lg">Panic Button</a>
            </div>
            @endif
            <!-- Default to the left -->
            <strong>Copyright &copy; 2021 <a href="#">kuli.it</a>.</strong>
        </footer>
        @else
        @yield('content')
        @endif
    </div>
</body>

<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->
<script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>
<!-- Bootstrap 4 -->
<script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('dist/js/adminlte.min.js') }}"></script>
@yield('js')
<script>
    $(document).ready(function(){
        $('#pnc').click(() => {
            $('#pnc').hide();
        });
        $('#panic').click(() => {
            $('#pnc').show();
        });
    })
</script>

</html>