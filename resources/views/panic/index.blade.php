@extends('layouts.app')

@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="card-header">Panic</div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nama</th>
                            <th>Dinas</th>
                            <th>Alamat</th>
                            <th>No Hp</th>
                            <th>Tgl</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($panic as $item)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $item->user->name }}</td>
                            <td>{{ $item->user->dinas }}</td>
                            <td>{{ $item->user->alamat }}</td>
                            <td>{{ $item->user->no_hp }}</td>
                            <td>{{ $item->created_at }}</td>
                            <td>
                                @switch($item->status)
                                @case(1)
                                <div class="btn btn-success btn-sm"
                                    style="cursor: auto;">PIRATA</div>
                                @break
                                @default
                                <div class="btn btn-danger btn-sm"
                                    style="cursor: auto;">DARURAT</div>

                                @endswitch
                            </td>
                            <td>
                                <a class="btn btn-outline-success" href="https://api.whatsapp.com/send?phone=62{{ (Auth::user()->admin == 1) ? $item->user->no_hp : '8123354813' }}&text={{ (Auth::user()->admin == 1) ? '' : 'Saya membutuhkan bantuan segera' }}"><i class="fab fa-whatsapp"></i> Chat WA</a>
                                @if ($item->status != 1 && Auth::user()->admin == 1)
                                <form method="POST" action="{{ url('/panic' . '/' . $item->id) }}"
                                    accept-charset="UTF-8" style="display:inline">
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}
                                    <button type="submit" class="btn btn-success btn-sm"
                                        onclick="return confirm(&quot;Pirata?&quot;)"><i class="fa fa-ambulance"
                                            aria-hidden="true"></i> Pirata</button>
                                </form>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="pagination-wrapper"> {!! $panic->appends(['search' => Request::get('search')])->render() !!}
                </div>
            </div>

        </div>
    </div>
</div>
@endsection