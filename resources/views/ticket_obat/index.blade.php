@extends('layouts.app')

@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="card-header">Ticket</div>
        <div class="card-body">
            <a href="{{ url('/ticket_pengobatan/create') }}" class="btn btn-success btn-sm" title="Add New Ticket">
                <i class="fa fa-plus" aria-hidden="true"></i> Tambah baru
            </a>

            <form method="GET" action="{{ url('/ticket_pengobatan') }}" accept-charset="UTF-8"
                class="form-inline my-2 my-lg-0 float-right" role="search">
                <div class="input-group">
                    <input type="text" class="form-control" name="search" placeholder="Search..."
                        value="{{ request('search') }}">
                    <span class="input-group-append">
                        <button class="btn btn-secondary" type="submit">
                            <i class="fa fa-search"></i>
                        </button>
                    </span>
                </div>
            </form>

            <br />
            <br />
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nama Pasien</th>
                            <th>Judul</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($ticket as $item)
                        <tr>
                            <td style="white-space: nowrap;">{{ $loop->iteration }}</td>
                            <td style="white-space: nowrap;">{{ $item->user->name ?? 'Unknows' }}</td>
                            <td>{{ $item->judul }}</td>
                            <td style="white-space: nowrap;">
                                @switch($item->status)
                                @case(1)
                                <div class="btn btn-success btn-sm" style="cursor: auto;">Buka</div>
                                @break

                                @case(2)
                                <div class="btn btn-danger btn-sm" style="cursor: auto;">Tutup</div>
                                @break

                                @default
                                <div class="btn btn-warning btn-sm" style="cursor: auto;">Belum di baca</div>

                                @endswitch
                                @php
                                $belum = 0;
                                foreach ($item->message as $value) {
                                if ($value->read == 0 && $value->user_id == $item->user_id) {
                                $belum++;
                                }
                                }
                                @endphp
                            </td>
                            <td style="white-space: nowrap;">
                                <a href="{{ url('/ticket_pengobatan/' . $item->id) }}" title="View Ticket"
                                    class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i>
                                    @if ($belum > 0)
                                    <span class="badge badge-danger">{{ $belum }}</span>
                                    @endif
                                </a>

                                @if($item->status != 2)
                                <form method="POST" action="{{ url('ticket_pengobatan' . '/' . $item->id) }}"
                                    accept-charset="UTF-8" style="display:inline">
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}
                                    <button type="submit" class="btn btn-danger btn-sm" title="Close Ticket"
                                        onclick="return confirm(&quot;Anda yakin akan menutup percakapan ini?&quot;)"><i
                                            class="fa fa-times-circle" aria-hidden="true"></i> tutup</button>
                                </form>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="pagination-wrapper"> {!! $ticket->appends(['search' => Request::get('search')])->render()
                    !!} </div>
            </div>

        </div>
    </div>
</div>
@endsection