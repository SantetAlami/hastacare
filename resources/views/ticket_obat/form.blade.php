<div class="form-group {{ $errors->has('judul') ? 'has-error' : ''}}">
    <label for="judul" class="control-label">{{ 'Judul' }}</label>
    <input class="form-control" name="judul" type="text" id="judul" value="{{ isset($ticket->judul) ? $ticket->judul : ''}}" >
    {!! $errors->first('judul', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>
