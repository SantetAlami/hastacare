@extends('layouts.app')

@section('content')
<div class="col-md-6">
    {{-- <div class="card">
        <div class="card-header">Ticket {{ $ticket->id }}</div>
<div class="card-body">

    <a href="{{ url('/ticket') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left"
                aria-hidden="true"></i> Back</button></a>
    <a href="{{ url('/ticket/' . $ticket->id . '/edit') }}" title="Edit Ticket"><button
            class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
            Edit</button></a>

    <form method="POST" action="{{ url('ticket' . '/' . $ticket->id) }}" accept-charset="UTF-8" style="display:inline">
        {{ method_field('DELETE') }}
        {{ csrf_field() }}
        <button type="submit" class="btn btn-danger btn-sm" title="Close Ticket"
            onclick="return confirm(&quot;Anda yakin akan menutup percakapan ini?&quot;)"><i class="fa fa-times-circle"
                aria-hidden="true"></i> tutup</button>
    </form>
    <br />
    <br />

    <div class="table-responsive">
        <table class="table">
            <tbody>
                <th> Judul </th>
                <td> {{ $ticket->judul }} </td>
                </tr>
                <tr>
            </tbody>
        </table>
    </div> --}}

    {{-- <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <th>Nama</th><th>Message</th>
                        </thead>
                        <tbody>
                            @foreach ($ticket->message as $item)
                                <tr>
                                    <td>{{ $item->user->name }}</td>
    <td>{{ $item->message }}</td>
    </tr>
    @endforeach
    </tbody>
    </table>
</div> --}}

{{-- <chat-messages :messages="messages" :messagesdb="{{ $ticket->message }}" :ticket_id="{{ $ticket->id }}"
:user_id="{{ Auth::user()->id }}"></chat-messages> --}}
{{-- <chat-form
                        v-on:messagesent="addMessage"
                        :user="{{ Auth::user() }}"
></chat-form> --}}
{{-- @include ('message.create', ['formMode' => 'create', 'ticket_id' => $ticket->id])

        </div>
    </div> --}}
<div class="card direct-chat direct-chat-primary">
    <div class="card-header">
        <h3 class="card-title">{{ $ticket->judul }}</h3>
        <div class="card-tools">
            <a href="{{ url('/ticket_kesehatan') }}" title="Back"><button class="btn btn-outline-warning btn-sm"><i
                        class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
            @if ($ticket->status != 2)
            <a href="{{ url('/ticket_kesehatan/' . $ticket->id . '/edit') }}" title="Edit Ticket"><button
                    class="btn btn-outline-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                    Edit</button></a>

            <form method="POST" action="{{ url('ticket_kesehatan' . '/' . $ticket->id) }}" accept-charset="UTF-8"
                style="display:inline">
                {{ method_field('DELETE') }}
                {{ csrf_field() }}
                <button type="submit" class="btn btn-danger btn-sm" title="Close Ticket"
                    onclick="return confirm(&quot;Anda yakin akan menutup percakapan ini?&quot;)"><i
                        class="fa fa-times-circle" aria-hidden="true"></i> tutup</button>
            </form>
            @endif
        </div>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <!-- Conversations are loaded here -->
        <chat-messages :messages="messages" :messagesdb="{{ $ticket->message }}" :ticket_id="{{ $ticket->id }}"
            :user_id="{{ Auth::user()->id }}" :photo="'{{ asset('dist/img/user3-128x128.jpg') }}'"></chat-messages>
        <!--/.direct-chat-messages-->
    </div>
    <!-- /.card-body -->
    @if ($ticket->status != 2)
    <div class="card-footer">
        <form class="input-group" action="javascript:void(0)" id="formMessage">
            {{-- <form class="input-group" method="post" action="{{ url('/message') }}"> --}}
            {{ csrf_field() }}
            <input type="hidden" name="ticket_id" value="{{ $ticket->id }}" />
            <input id="newMessage" type="text" name="message" placeholder="Type Message ..." class="form-control" />
            <span class="input-group-append">
                <button type="submit" class="btn btn-primary" id="btn-chat">
                    Send
                </button>
            </span>
        </form>
        {{-- <chat-form v-on:messagesent="addMessage" :user="{{ Auth::user() }}"></chat-form> --}}
    </div>
    <!-- /.card-footer-->
    @endif
</div>
</div>
<div class="col-md-6">
    <div class="card card-primary card-outline">
        <div class="card-header">Riwayat Kesehatan</div>
        <div class="card-body">
            <p>
                <b>Nama :</b> {{ $ticket->user->name }}
            </p>
            <p>
                <b>Unit :</b> {{ $ticket->user->unit }}
            </p>
            <p>
                <b>Dinas :</b> {{ $ticket->user->dinas }}
            </p>
            <p>
                <b>No HP :</b> {{ $ticket->user->no_hp }}
            </p>
            <div class="table-responsive" id="table">
                <table class="table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nama Gejala</th>
                            <th>Deskripsi</th>
                            <th>Tanggal Gejala</th>
                            <th>Tag</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($riwayat as $item)
                        <tr>
                            <td style="white-space: nowrap;">{{ $loop->iteration }}</td>
                            <td style="white-space: nowrap;">{{ $item->judul }}</td>
                            <td>{{ $item->deskripsi }}</td>
                            <td>{{ $item->tgl }}</td>
                            <td>{{ $item->tag }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                {{-- <div class="pagination-wrapper"> {!! $riwayat->appends(['search' => Request::get('search')])->render()
                    !!} </div> --}}
            </div>

        </div>
    </div>
</div>
@endsection

@section('style')
  <!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
  <link rel="stylesheet" href="{{ asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
  <link rel="stylesheet" href="{{ asset('plugins/datatables-buttons/css/buttons.bootstrap4.min.css')}}">
@endsection

@section('js')
<!-- jQuery -->
<script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>
<!-- Bootstrap 4 -->
<script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('dist/js/adminlte.min.js') }}"></script>
<!-- DataTables  & Plugins -->
<script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('plugins/jszip/jszip.min.js') }}"></script>
<script src="{{ asset('plugins/pdfmake/pdfmake.min.js') }}"></script>
<script src="{{ asset('plugins/pdfmake/vfs_fonts.js') }}"></script>
<script src="{{ asset('plugins/datatables-buttons/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-buttons/js/buttons.print.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-buttons/js/buttons.colVis.min.js') }}"></script>
<script>
    $(document).ready(function(){
        $("#formMessage").submit(function(){
            var message = $('#newMessage').val()
            $('#btn-chat').prop('disabled', true)
            $.post("{{ url('/messages') }}",
            {
                "_token": "{{ csrf_token() }}",
                "message": message,
                "ticket_id": "{{ $ticket->id }}"
            },
            function(data,status){
                // console.log(data,status);
                $('#newMessage').val('')
                $('#btn-chat').prop('disabled', false)
            });
        });
        $('#table').DataTable({
            "paging": false,
            "lengthChange": false,
            "searching": true,
            "ordering": true,
            "info": false,
            "autoWidth": false,
            "responsive": false,
        });
    });
    var timer = 100;
    var heigth = 0;
    var checkExist = setInterval(function() {
        if ($('#dm').length && timer == 100) {
            var scrollDiv = document.getElementById("dm");
            scrollDiv.scrollTop = scrollDiv.scrollHeight;
            timer = 0
        }
        if ($('#dm').length) {
            var scrollDiv = document.getElementById("dm");
            if(heigth != scrollDiv.scrollHeight) {
                heigth = scrollDiv.scrollTop = scrollDiv.scrollHeight;
            }
            timer = 0
        }

    }, timer); // check every 100ms

</script>
@endsection