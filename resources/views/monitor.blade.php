@extends('layouts.app')

@section('content')
<div class="col-12">
    <div class="card">
        <div class="card-header border-0">
            <div class="d-flex justify-content-between">
                <h3 class="card-title">10 besar user teraktif per bulan</h3>
            </div>
        </div>
        <div class="card-body">
            <div class="d-flex">
                <p class="d-flex flex-column">
                    <span class="text-bold text-lg">300.000</span>
                    <span>Total keaktifan kunjungan</span>
                </p>
                {{-- <p class="ml-auto d-flex flex-column text-right">
                    <span class="text-success">
                        <i class="fas fa-arrow-up"></i> 33.1%
                    </span>
                    <span class="text-muted">Since last month</span>
                </p> --}}
            </div>
            <!-- /.d-flex -->

            <div class="position-relative">
                <canvas id="sales-chart" height="500"></canvas>
            </div>

            <div class="d-flex flex-row justify-content-end">
                <span class="mr-2">
                    <i class="fas fa-square text-primary"></i> Keaktifan Total
                </span>

                <span>
                    <i class="fas fa-square text-gray"></i> Keaktifan Chating
                </span>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<!-- OPTIONAL SCRIPTS -->
<script src="{{ asset('plugins/chart.js/Chart.min.js') }}"></script>
<script>
    $(function () {
        var ticksStyle = {
            fontColor: '#495057',
            fontStyle: 'bold'
        }

        var mode = 'index'
        var intersect = true

        var $salesChart = $('#sales-chart')
        // eslint-disable-next-line no-unused-vars
        var salesChart = new Chart($salesChart, {
            type: 'bar',
            data: {
            labels: {!! json_encode($name) !!},
            datasets: [
                {
                backgroundColor: '#007bff',
                borderColor: '#007bff',
                data: {!! json_encode($total) !!}
                },
                {
                backgroundColor: '#ced4da',
                borderColor: '#ced4da',
                data: {!! json_encode($chating) !!}
                }
            ]
            },
            options: {
            maintainAspectRatio: false,
            tooltips: {
                mode: mode,
                intersect: intersect
            },
            hover: {
                mode: mode,
                intersect: intersect
            },
            legend: {
                display: false
            },
            scales: {
                yAxes: [{
                // display: false,
                gridLines: {
                    display: true,
                    lineWidth: '4px',
                    color: 'rgba(0, 0, 0, .2)',
                    zeroLineColor: 'transparent'
                },
                ticks: $.extend({
                    beginAtZero: true,

                    // Include a dollar sign in the ticks
                    callback: function (value) {
                    if (value >= 1000) {
                        value /= 1000
                        value += 'rb'
                    }

                    return value
                    }
                }, ticksStyle)
                }],
                xAxes: [{
                display: true,
                gridLines: {
                    display: false
                },
                ticks: ticksStyle
                }]
            }
            }
        })
    })
</script>
@endsection