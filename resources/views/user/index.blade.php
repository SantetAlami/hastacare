@extends('layouts.app')

@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="card-header">User</div>
        <div class="card-body">
            <a href="{{ url('/master/user/create') }}" class="btn btn-success btn-sm" title="Add New User">
                <i class="fa fa-plus" aria-hidden="true"></i> Add New
            </a>

            <form method="GET" action="{{ url('/master/user') }}" accept-charset="UTF-8"
                class="form-inline my-2 my-lg-0 float-right" role="search">
                <div class="input-group">
                    <input type="text" class="form-control" name="search" placeholder="Search..."
                        value="{{ request('search') }}">
                    <span class="input-group-append">
                        <button class="btn btn-secondary" type="submit">
                            <i class="fa fa-search"></i>
                        </button>
                    </span>
                </div>
            </form>

            <br />
            <br />
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Foto</th>
                            <th>Name</th>
                            <th>NRP</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($user as $item)
                        <tr style="white-space: nowrap">
                            <td>{{ $loop->iteration }}</td>
                            <td>
                                <img style="width: 150px" src="{{ url("/foto" . "/" . $item->foto) }}" alt="" srcset="">
                            </td>
                            <td>{{ $item->name }}</td>
                            <td>{{ $item->username }}</td>
                            <td>
                                <a href="{{ url('/master/user/' . $item->id) }}" title="View User"><button
                                        class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i></button></a>
                                <a href="{{ url('/master/user/' . $item->id . '/edit') }}" title="Edit User"><button
                                        class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o"
                                            aria-hidden="true"></i></button></a>

                                <form method="POST" action="{{ url('/master/user' . '/' . $item->id) }}"
                                    accept-charset="UTF-8" style="display:inline">
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}
                                    <button type="submit" class="btn btn-danger btn-sm" title="Delete User"
                                        onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash"
                                            aria-hidden="true"></i></button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="pagination-wrapper"> {!! $user->appends(['search' => Request::get('search')])->render() !!}
                </div>
            </div>

        </div>
    </div>
</div>
@endsection