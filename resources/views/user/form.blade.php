<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
    <label for="name" class="control-label">{{ 'Name' }}</label>
    <input class="form-control" name="name" type="text" id="name" value="{{ isset($user->name) ? $user->name : ''}}">
    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('username') ? 'has-error' : ''}}">
    <label for="username" class="control-label">{{ 'Username' }}</label>
    <input class="form-control" name="username" type="text" id="username"
        value="{{ isset($user->username) ? $user->username : ''}}">
    {!! $errors->first('username', '<p class="help-block">:message</p>') !!}
</div>
{{-- <div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
    <label for="email" class="control-label">{{ 'Email' }}</label>
    <input class="form-control" name="email" type="email" id="email"
        value="{{ isset($user->email) ? $user->email : ''}}">
    {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
</div> --}}
<div class="form-group {{ $errors->has('pangkat') ? 'has-error' : ''}}">
    <label for="pangkat" class="control-label">{{ 'Pangkat' }}</label>
    <input class="form-control" name="pangkat" type="text" id="pangkat"
        value="{{ isset($user->pangkat) ? $user->pangkat : ''}}">
    {!! $errors->first('pangkat', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group">
    <label for="unit" class="control-label">{{ __('Unit') }}</label>

    <select name="unit" id="unit" class="form-control @error('unit') is-invalid @enderror" name="unit" required>
        <option value="Bagops" {{ Auth::user()->unit == 'Bagops' ? 'selected' : '' }}>Bagops</option>
        <option value="Bagren" {{ Auth::user()->unit == 'Bagren' ? 'selected' : '' }}>Bagren</option>
        <option value="Bagsumda" {{ Auth::user()->unit == 'Bagsumda' ? 'selected' : '' }}>Bagsumda</option>
        <option value="Brimob Malang" {{ Auth::user()->unit == 'Brimob Malang' ? 'selected' : '' }}>Brimob Malang
        </option>
        <option value="Polsek" {{ Auth::user()->unit == 'Polsek' ? 'selected' : '' }}>Polsek</option>
        <option value="Rumkit" {{ Auth::user()->unit == 'Rumkit' ? 'selected' : '' }}>Rumkit</option>
        <option value="Satintelkam" {{ Auth::user()->unit == 'Satintelkam' ? 'selected' : '' }}>Satintelkam</option>
        <option value="Satlantas" {{ Auth::user()->unit == 'Satlantas' ? 'selected' : '' }}>Satlantas</option>
        <option value="Satreskrim" {{ Auth::user()->unit == 'Satreskrim' ? 'selected' : '' }}>Satreskrim</option>
        <option value="Satresnarkoba" {{ Auth::user()->unit == 'Satresnarkoba' ? 'selected' : '' }}>Satresnarkoba
        </option>
        <option value="Satsabhara" {{ Auth::user()->unit == 'Satsabhara' ? 'selected' : '' }}>Satsabhara</option>
        <option value="Sattahti" {{ Auth::user()->unit == 'Sattahti' ? 'selected' : '' }}>Sattahti</option>
        <option value="Sikeu" {{ Auth::user()->unit == 'Sikeu' ? 'selected' : '' }}>Sikeu</option>
        <option value="Sipropam" {{ Auth::user()->unit == 'Sipropam' ? 'selected' : '' }}>Sipropam</option>
        <option value="Sitipol" {{ Auth::user()->unit == 'Sitipol' ? 'selected' : '' }}>Sitipol</option>
        <option value="Siwas" {{ Auth::user()->unit == 'Siwas' ? 'selected' : '' }}>Siwas</option>
    </select>

    {!! $errors->first('unit', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group">
    <label for="satuan" class="control-label">{{ __('Satuan') }}</label>

    <select name="satuan" id="satuan" class="form-control @error('satuan') is-invalid @enderror" name="satuan" disabled>
        <option value="Polres Batu" {{ Auth::user()->satuan == 'Polres Batu' ? 'selected' : '' }}>Polres Batu
        </option>
        <option value="Polres Malang" {{ Auth::user()->satuan == 'Polres Malang' ? 'selected' : '' }}>Polres Malang
        </option>
        <option value="Polresta Malang Kota" {{ Auth::user()->satuan == 'Polresta Malang Kota' ? 'selected' : '' }}>
            Polresta Malang Kota</option>
        <option value="RSB Hasta  Brata Batu" {{ Auth::user()->satuan == 'RSB Hasta  Brata Batu' ? 'selected' : '' }}>
            RSB Hasta Brata Batu</option>
        <option value="Satbrimob Malang" {{ Auth::user()->satuan == 'Satbrimob Malang' ? 'selected' : '' }}>
            Satbrimob Malang</option>
    </select>

    {!! $errors->first('satuan', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('dinas') ? 'has-error' : ''}}">
    <label for="dinas" class="control-label">{{ 'Dinas' }}</label>
    <input class="form-control" name="dinas" type="text" id="dinas"
        value="{{ isset($user->dinas) ? $user->dinas : ''}}">
    {!! $errors->first('dinas', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('no_hp') ? 'has-error' : ''}}">
    <label for="no_hp" class="control-label">{{ 'No Hp' }}</label>
    <input class="form-control" name="no_hp" type="text" id="no_hp"
        value="{{ isset($user->no_hp) ? $user->no_hp : ''}}">
    {!! $errors->first('no_hp', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('alamat') ? 'has-error' : ''}}">
    <label for="alamat" class="control-label">{{ 'Alamat' }}</label>
    <input class="form-control" name="alamat" type="text" id="alamat"
        value="{{ isset($user->alamat) ? $user->alamat : ''}}">
    {!! $errors->first('alamat', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('admin') ? 'has-error' : ''}}">
    <label for="admin" class="control-label">{{ 'Admin' }}</label>
    <input class="icheck-success d-inline" name="admin" type="checkbox" id="admin"
        {{ isset($user->admin) && $user->admin == 1 ? 'checked' : ''}} value=1>
    {!! $errors->first('admin', '<p class="help-block">:message</p>') !!}
</div>
{{-- <div class="form-group {{ $errors->has('foto') ? 'has-error' : ''}}">
    <label for="foto" class="control-label">{{ 'Foto' }}</label>
    <input class="form-control" name="foto" type="file" id="foto" value="{{ isset($user->foto) ? $user->foto : ''}}">
    {!! $errors->first('foto', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('foto_gigi') ? 'has-error' : ''}}">
    <label for="foto_gigi" class="control-label">{{ 'Foto Gigi' }}</label>
    <input class="form-control" name="foto_gigi" type="file" id="foto_gigi"
        value="{{ isset($user->foto_gigi) ? $user->foto_gigi : ''}}">
    {!! $errors->first('foto_gigi', '<p class="help-block">:message</p>') !!}
</div> --}}
<div class="form-group {{ $errors->has('password') ? 'has-error' : ''}}">
    <label for="password" class="control-label">{{ 'Password' }}</label>
    <input class="form-control" name="password" type="password" id="password">
    {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>