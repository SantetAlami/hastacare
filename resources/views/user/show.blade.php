@extends('layouts.app')

@section('content')
<div class="col-md-3">
    <div class="card card-primary card-outline">
        <div class="card-body box-profile">
            <div class="text-center">
                <div class="profile-user-img img-fluid img-circle" style="height: 100px">
                    <i class="fa fa-user" style="font-size: 80px"></i>
                </div>
                {{-- <img class="profile-user-img img-fluid img-circle" src="../../dist/img/user4-128x128.jpg"
                    alt="User profile picture"> --}}
            </div>

            <h3 class="profile-username text-center">{{ $user->name }}</h3>

            <p class="text-muted text-center">{{ $user->pangkat }} / {{ $user->username }} <br> {{ $user->unit }} -
                {{ $user->satuan }}</p>

            <ul class="list-group list-group-unbordered mb-3">
                <li class="list-group-item">
                    <b>Dinas</b> <a class="float-right">{{ $user->dinas }}</a>
                </li>
                <li class="list-group-item">
                    <b>No Hp</b> <a class="float-right">{{ $user->no_hp }}</a>
                </li>
                <li class="list-group-item">
                    <b>Alamat</b> <a class="float-right">{{ $user->alamat }}</a>
                </li>
            </ul>

            <a href="{{ url('/master/user/' . $user->id . '/edit') }}" class="btn btn-primary btn-block"><b>Ubah</b></a>
        </div>
        <!-- /.card-body -->
    </div>
</div>
<div class="col-md-9">
    <div class="card card-primary card-outline">
        <div class="card-header">Odontogram</div>
        <div class="card-body row">
            <div class="col-md-4 p-3">
                <form action="{{ url('upload_foto_gigi') }}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <label for="up">Foto gigi</label>
                    <div class="input-group mb-2">
                        <input id="up" type="file" class="form-control" name="up" capture="camera">
                        <span class="input-group-append">
                            <button class="btn btn-success" type="submit">
                                <i class="fa fa-upload"></i>
                            </button>
                        </span>
                    </div>
                </form>
                <img src="{{ url('foto_gigi') . '/' . (!empty($user->foto_gigi) ? $user->foto_gigi : 'default.jpg') }}" alt="" srcset=""
                    style="width: 100%; max-height: 100%">
            </div>
            <div class="col-md-8 border-right">
                <form action="{{ '/ubah_odontogram/' . $user->id }}" method="post" class="row">
                    {{ csrf_field() }}
                    <div class="form-group col-md-4">
                        <label for="no" class="control-label">No Gigi</label>
                        {{-- <input class="form-control" name="no" type="number" id="no" value=""> --}}
                        <select name="no" id="no" class="form-control select2 @error('no') is-invalid @enderror"
                            name="status">
                            <option value="" selected disabled>--Pilih no gigi--</option>
                            <option value="" disabled>Atas kanan</option>
                            <option value="18">18</option>
                            <option value="17">17</option>
                            <option value="16">16</option>
                            <option value="15">15</option>
                            <option value="14">14</option>
                            <option value="13">13</option>
                            <option value="12">12</option>
                            <option value="11">11</option>
                            <option value="" disabled>Atas kiri</option>
                            <option value="21">21</option>
                            <option value="22">22</option>
                            <option value="23">23</option>
                            <option value="24">24</option>
                            <option value="25">25</option>
                            <option value="26">26</option>
                            <option value="27">27</option>
                            <option value="28">28</option>
                            <option value="" disabled>Atas kanan tambahan</option>
                            <option value="55">55</option>
                            <option value="54">54</option>
                            <option value="53">53</option>
                            <option value="52">52</option>
                            <option value="51">51</option>
                            <option value="" disabled>Atas kiri tambahan</option>
                            <option value="61">61</option>
                            <option value="62">62</option>
                            <option value="63">63</option>
                            <option value="64">64</option>
                            <option value="65">65</option>
                            <option value="" disabled>Bawah kanan tambahan</option>
                            <option value="85">85</option>
                            <option value="84">84</option>
                            <option value="83">83</option>
                            <option value="82">82</option>
                            <option value="81">81</option>
                            <option value="" disabled>Bawah kiri tambahan</option>
                            <option value="71">71</option>
                            <option value="72">72</option>
                            <option value="73">73</option>
                            <option value="74">74</option>
                            <option value="75">75</option>
                            <option value="" disabled>Bawah kanan</option>
                            <option value="48">48</option>
                            <option value="47">47</option>
                            <option value="46">46</option>
                            <option value="45">45</option>
                            <option value="44">44</option>
                            <option value="43">43</option>
                            <option value="42">42</option>
                            <option value="41">41</option>
                            <option value="" disabled>Bawah kiri</option>
                            <option value="31">31</option>
                            <option value="32">32</option>
                            <option value="33">33</option>
                            <option value="34">34</option>
                            <option value="35">35</option>
                            <option value="36">36</option>
                            <option value="37">37</option>
                            <option value="38">38</option>
                        </select>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="status" class="control-label">Status</label>
                        <select name="status" id="status" class="form-control @error('status') is-invalid @enderror"
                            name="status">
                            <option value="white|Sehat">Sehat</option>
                            <option value="red|Lubang">Lubang</option>
                            <option value="black|Hilang">Hilang</option>
                            <option value="green|Anak">Anak</option>
                            <option value="blue|Dewasa">Dewasa</option>
                            <option value="pink|Jaringan tdk sehat">Jaringan tdk sehat</option>
                            <option value="gray|Akar Gigi">Akar Gigi</option>
                            <option value="yellow|Palsu">Palsu</option>
                            <option value="wheat|Tidak tumbuh">Tidak tumbuh</option>
                            <option value="chocolate|Gusi tdk sehat">Gusi tdk sehat</option>
                        </select>
                    </div>
                    <div class="form-group col-md-2">
                        <label for="satuan" class="control-label" style="color: transparent">a</label>
                        <button class="form-control btn btn-primary" type="submit"><i class="fa fa-check"></i></button>
                    </div>
                </form>
                <div style="overflow-x: scroll">
                    @include('riwayat.odotogram')
                </div>
            </div>
        </div>
    </div>
    <div class="card card-primary card-outline">
        <div class="card-header">Riwayat Kesehatan</div>
        <div class="card-body">
            @if(Auth::user()->id == $user->id)
            <div class="row">
                <div class="col-4">
                    <a href="{{ route('riwayat.create') }}" class="btn btn-success btn-sm " title="Add New Riwayat">
                        <i class="fa fa-plus" aria-hidden="true"></i> Add New
                    </a>
                </div>
            </div>
            @endif

            <br />
            <br />
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nama Gejala</th>
                            <th>Deskripsi</th>
                            <th>Tanggal Gejala</th>
                            <th>Tag</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($riwayat as $item)
                        <tr>
                            <td style="white-space: nowrap;">{{ $loop->iteration }}</td>
                            <td style="white-space: nowrap;">{{ $item->judul }}</td>
                            <td>{{ $item->deskripsi }}</td>
                            <td>{{ $item->tgl }}</td>
                            <td>{{ $item->tag }}</td>
                            <td style="white-space: nowrap;">
                                {{-- <a href="{{ url('/riwayat/' . $item->id) }}" title="View Riwayat"><button
                                    class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i>
                                    View</button></a> --}}
                                <a href="{{ url('/profil_kesehatan/' . $item->id . '/edit') }}"
                                    title="Edit Riwayat"><button class="btn btn-primary btn-sm"><i
                                            class="fa fa-pencil-square-o" aria-hidden="true"></i> Ubah</button></a>

                                <form method="POST" action="{{ url('/profil_kesehatan' . '/' . $item->id) }}"
                                    accept-charset="UTF-8" style="display:inline">
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}
                                    <button type="submit" class="btn btn-danger btn-sm" title="Delete Riwayat"
                                        onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash"
                                            aria-hidden="true"></i></button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="pagination-wrapper"> {!! $riwayat->appends(['search' => Request::get('search')])->render()
                    !!} </div>
            </div>

        </div>
    </div>
</div>
@endsection

@section('style')
<link rel="stylesheet" href="{{ asset('plugins/select2/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
@endsection

@section('js')
<script src="{{ asset('plugins/select2/js/select2.full.min.js') }}"></script>
<script>
    $(document).ready(function(){
        $('.select2').select2()

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#up").change(function(){
            readURL(this);
        });
    })
</script>
@endsection