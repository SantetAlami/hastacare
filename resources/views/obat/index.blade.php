@extends('layouts.app')

@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="card-header">Obat</div>
        <div class="card-body">
            @if (Auth::user()->admin == 1)
            <a href="{{ url('/ketersediaan_obat/create') }}" class="btn btn-success btn-sm" title="Add New Obat">
                <i class="fa fa-plus" aria-hidden="true"></i> Add New
            </a>
            @endif

            <form method="GET" action="{{ url('/ketersediaan_obat') }}" accept-charset="UTF-8"
                class="form-inline my-2 my-lg-0 float-right" role="search">
                <div class="input-group">
                    <input type="text" class="form-control" name="search" placeholder="Search..."
                        value="{{ request('search') }}">
                    <span class="input-group-append">
                        <button class="btn btn-secondary" type="submit">
                            <i class="fa fa-search"></i>
                        </button>
                    </span>
                </div>
            </form>

            <br />
            <br />
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nama</th>
                            <th>Satuan</th>
                            <th>Stock</th>
                            @if (Auth::user()->admin == 1)
                            <th>Actions</th>
                            @endif
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($obat as $item)
                        <tr>
                            <td>{{ $loop->iteration + ((request('page')-1) * 25) }}</td>
                            <td>{{ $item->nama }}</td>
                            <td>{{ $item->satuan }}</td>
                            <td>{{ $item->stock }}</td>
                            @if (Auth::user()->admin == 1)
                            <td>
                                <a href="{{ url('/ketersediaan_obat/' . $item->id) }}" title="View Obat"><button
                                        class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i>
                                        View</button></a>
                                <a href="{{ url('/ketersediaan_obat/' . $item->id . '/edit') }}" title="Edit Obat"><button
                                        class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o"
                                            aria-hidden="true"></i> Edit</button></a>

                                <form method="POST" action="{{ url('/ketersediaan_obat' . '/' . $item->id) }}" accept-charset="UTF-8"
                                    style="display:inline">
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}
                                    <button type="submit" class="btn btn-danger btn-sm" title="Delete Obat"
                                        onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o"
                                            aria-hidden="true"></i> Delete</button>
                                </form>
                            </td>
                            @endif
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="pagination-wrapper"> {!! $obat->appends(['search' => Request::get('search')])->render() !!}
                </div>
            </div>

        </div>
    </div>
</div>
@endsection