@extends('layouts.app')

@section('content')
<div class="col-md-12">
    <div class="card card-primary card-outline">
        <div class="card-header">Ketersediaan Kamar</div>
        <div class="card-body">
            <a href="{{ url('/sarana_dan_pelayanan') }}" title="Back"><button class="btn btn-outline-warning btn-sm"><i
                        class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
            @if (Auth::user()->admin == 1)
            <a href="{{ url('/ketersediaan_kamar/create') }}" class="btn btn-success btn-sm"
                title="Add New KetersediaanKamar">
                <i class="fa fa-plus" aria-hidden="true"></i> Add New
            </a>
            @endif

            <form method="GET" action="{{ url('/ketersediaan_kamar') }}" accept-charset="UTF-8"
                class="form-inline my-2 my-lg-0 float-right" role="search">
                <div class="input-group">
                    <input type="text" class="form-control" name="search" placeholder="Search..."
                        value="{{ request('search') }}">
                    <span class="input-group-append">
                        <button class="btn btn-secondary" type="submit">
                            <i class="fa fa-search"></i>
                        </button>
                    </span>
                </div>
            </form>

            <br />
            <br />
            <div class="table-responsive table-striped table-bordered">
                <table class="table">
                    <thead style="text-align: center;text-verical-align: center">
                        <tr>
                            <th rowspan="2">#</th>
                            <th rowspan="2">Ruang</th>
                            <th colspan="3">Tempat Tidur</th>
                        </tr>
                        <tr>
                            <th style="background-color: yellow">Total</th>
                            <th style="background-color: red; color:white">Terisi</th>
                            <th style="background-color: green; color:white">Kosong</th>
                            @if (Auth::user()->admin == 1)
                            <th>Actions</th>
                            @endif
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($ketersediaankamar as $item)
                        <tr>
                            <td style="text-align: center;">{{ $loop->iteration }}</td>
                            <td>{{ $item->ruang }}</td>
                            <td style="text-align: center;">{{ $item->total }}</td>
                            <td style="text-align: center;">{{ $item->terisi }}</td>
                            <td style="text-align: center;">{{ $item->total - $item->terisi }}</td>
                            @if (Auth::user()->admin == 1)
                            <td style="text-align: center;">
                                <a href="{{ url('/ketersediaan_kamar/' . $item->id) }}"
                                    title="View KetersediaanKamar"><button class="btn btn-info btn-sm"><i
                                            class="fa fa-eye" aria-hidden="true"></i></button></a>
                                <a href="{{ url('/ketersediaan_kamar/' . $item->id . '/edit') }}"
                                    title="Edit KetersediaanKamar"><button class="btn btn-primary btn-sm"><i
                                            class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a>

                                <form method="POST" action="{{ url('/ketersediaan_kamar' . '/' . $item->id) }}"
                                    accept-charset="UTF-8" style="display:inline">
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}
                                    <button type="submit" class="btn btn-danger btn-sm" title="Delete KetersediaanKamar"
                                        onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash"
                                            aria-hidden="true"></i></button>
                                </form>
                            </td>
                            @endif
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="pagination-wrapper"> {!! $ketersediaankamar->appends(['search' =>
                    Request::get('search')])->render() !!} </div>
            </div>

        </div>
    </div>
</div>
@endsection