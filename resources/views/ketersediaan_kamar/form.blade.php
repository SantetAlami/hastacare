<div class="form-group {{ $errors->has('ruang') ? 'has-error' : ''}}">
    <label for="ruang" class="control-label">{{ 'Ruang' }}</label>
    <input class="form-control" name="ruang" type="text" id="ruang" value="{{ isset($ketersediaankamar->ruang) ? $ketersediaankamar->ruang : ''}}" >
    {!! $errors->first('ruang', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('total') ? 'has-error' : ''}}">
    <label for="total" class="control-label">{{ 'Total' }}</label>
    <input class="form-control" name="total" type="number" id="total" value="{{ isset($ketersediaankamar->total) ? $ketersediaankamar->total : ''}}" >
    {!! $errors->first('total', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('terisi') ? 'has-error' : ''}}">
    <label for="terisi" class="control-label">{{ 'Terisi' }}</label>
    <input class="form-control" name="terisi" type="number" id="terisi" value="{{ isset($ketersediaankamar->terisi) ? $ketersediaankamar->terisi : ''}}" >
    {!! $errors->first('terisi', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>
