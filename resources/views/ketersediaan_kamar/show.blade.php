@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">KetersediaanKamar {{ $ketersediaankamar->id }}</div>
                    <div class="card-body">

                        <a href="{{ url('/ketersediaan_kamar') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="{{ url('/ketersediaan_kamar/' . $ketersediaankamar->id . '/edit') }}" title="Edit KetersediaanKamar"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                        <form method="POST" action="{{ url('ketersediaankamar' . '/' . $ketersediaankamar->id) }}" accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-sm" title="Delete KetersediaanKamar" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                        </form>
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $ketersediaankamar->id }}</td>
                                    </tr>
                                    <tr><th> Ruang </th><td> {{ $ketersediaankamar->ruang }} </td></tr><tr><th> Tempat Tidur </th><td> {{ $ketersediaankamar->tempat_tidur }} </td></tr><tr><th> Terisi </th><td> {{ $ketersediaankamar->terisi }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
