@extends('layouts.app')

@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="card-header">{{ __('Ubah') }}</div>

        <div class="card-body">
            <a href="{{ url('profil_kesehatan') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
            <br />
            <br />

            @if ($errors->any())
                <ul class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif

            <form method="POST" action="{{ url('ubah_profil') }}">
                @csrf

                <div class="form-group row">
                    <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                    <div class="col-md-6">
                        <input id="name" type="text" class="form-control @error('name') is-invalid @enderror"
                            name="name" value="{{ old('name') ?? Auth::user()->name }}" required autocomplete="name" autofocus>

                        @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="username" class="col-md-4 col-form-label text-md-right">{{ __('NRP') }}</label>

                    <div class="col-md-6">
                        <input id="username" type="number"
                            class="form-control @error('username') is-invalid @enderror" name="username"
                            value="{{ old('username') ?? Auth::user()->username }}" required autocomplete="username" disabled>

                        @error('username')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="pangkat" class="col-md-4 col-form-label text-md-right">{{ __('Pangkat') }}</label>

                    <div class="col-md-6">
                        <input id="pangkat" type="text"
                            class="form-control @error('pangkat') is-invalid @enderror" name="pangkat"
                            value="{{ old('pangkat') ?? Auth::user()->pangkat }}" required autocomplete="pangkat" required>

                        @error('pangkat')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="unit" class="col-md-4 col-form-label text-md-right">{{ __('Unit') }}</label>

                    <div class="col-md-6">
                        <select name="unit" id="unit" class="form-control @error('unit') is-invalid @enderror"
                            name="unit" required>
                            <option value="Bagops" {{ Auth::user()->unit == 'Bagops' ? 'selected' : '' }}>Bagops</option>
                            <option value="Bagren" {{ Auth::user()->unit == 'Bagren' ? 'selected' : '' }}>Bagren</option>
                            <option value="Bagsumda" {{ Auth::user()->unit == 'Bagsumda' ? 'selected' : '' }}>Bagsumda</option>
                            <option value="Brimob Malang" {{ Auth::user()->unit == 'Brimob Malang' ? 'selected' : '' }}>Brimob Malang</option>
                            <option value="Polsek" {{ Auth::user()->unit == 'Polsek' ? 'selected' : '' }}>Polsek</option>
                            <option value="Rumkit" {{ Auth::user()->unit == 'Rumkit' ? 'selected' : '' }}>Rumkit</option>
                            <option value="Satintelkam" {{ Auth::user()->unit == 'Satintelkam' ? 'selected' : '' }}>Satintelkam</option>
                            <option value="Satlantas" {{ Auth::user()->unit == 'Satlantas' ? 'selected' : '' }}>Satlantas</option>
                            <option value="Satreskrim" {{ Auth::user()->unit == 'Satreskrim' ? 'selected' : '' }}>Satreskrim</option>
                            <option value="Satresnarkoba" {{ Auth::user()->unit == 'Satresnarkoba' ? 'selected' : '' }}>Satresnarkoba</option>
                            <option value="Satsabhara" {{ Auth::user()->unit == 'Satsabhara' ? 'selected' : '' }}>Satsabhara</option>
                            <option value="Sattahti" {{ Auth::user()->unit == 'Sattahti' ? 'selected' : '' }}>Sattahti</option>
                            <option value="Sikeu" {{ Auth::user()->unit == 'Sikeu' ? 'selected' : '' }}>Sikeu</option>
                            <option value="Sipropam" {{ Auth::user()->unit == 'Sipropam' ? 'selected' : '' }}>Sipropam</option>
                            <option value="Sitipol" {{ Auth::user()->unit == 'Sitipol' ? 'selected' : '' }}>Sitipol</option>
                            <option value="Siwas" {{ Auth::user()->unit == 'Siwas' ? 'selected' : '' }}>Siwas</option>
                        </select>

                        @error('unit')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="satuan" class="col-md-4 col-form-label text-md-right">{{ __('Satuan') }}</label>

                    <div class="col-md-6">
                        <select name="satuan" id="satuan"
                            class="form-control @error('satuan') is-invalid @enderror" name="satuan" disabled>
                            <option value="Polres Batu" {{ Auth::user()->satuan == 'Polres Batu' ? 'selected' : '' }}>Polres Batu</option>
                            <option value="Polres Malang" {{ Auth::user()->satuan == 'Polres Malang' ? 'selected' : '' }}>Polres Malang</option>
                            <option value="Polresta Malang Kota" {{ Auth::user()->satuan == 'Polresta Malang Kota' ? 'selected' : '' }}>Polresta Malang Kota</option>
                            <option value="RSB Hasta  Brata Batu" {{ Auth::user()->satuan == 'RSB Hasta  Brata Batu' ? 'selected' : '' }}>RSB Hasta  Brata Batu</option>
                            <option value="Satbrimob Malang" {{ Auth::user()->satuan == 'Satbrimob Malang' ? 'selected' : '' }}>Satbrimob Malang</option>
                        </select>

                        @error('satuan')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="dinas"
                        class="col-md-4 col-form-label text-md-right">{{ __('Lokasi Dinas') }}</label>

                    <div class="col-md-6">
                        <input id="dinas" type="text" class="form-control @error('dinas') is-invalid @enderror"
                            name="dinas" value="{{ old('dinas') ?? Auth::user()->dinas }}" required autocomplete="dinas">

                        @error('dinas')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="no_hp" class="col-md-4 col-form-label text-md-right">{{ __('No Hp') }}</label>

                    <div class="col-md-6">
                        <input id="no_hp" type="text" class="form-control @error('no_hp') is-invalid @enderror"
                            name="no_hp" value="{{ old('no_hp') ?? Auth::user()->no_hp }}" required autocomplete="no_hp">

                        @error('no_hp')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="alamat" class="col-md-4 col-form-label text-md-right">{{ __('Alamat') }}</label>

                    <div class="col-md-6">
                        <input id="alamat" type="text"
                            class="form-control @error('alamat') is-invalid @enderror" name="alamat"
                            value="{{ old('alamat') ?? Auth::user()->alamat }}" required autocomplete="alamat">

                        @error('alamat')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="password"
                        class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                    <div class="col-md-6">
                        <input id="password" type="password"
                            class="form-control @error('password') is-invalid @enderror" name="password"
                            autocomplete="new-password" placeholder="(Kosongi saja jika tidak ingin mengubah password.)">

                        @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="password-confirm"
                        class="col-md-4 col-form-label text-md-right">{{ __('Password Konfirmasi') }}</label>

                    <div class="col-md-6">
                        <input id="password-confirm" type="password" class="form-control"
                            name="password_confirmation" autocomplete="new-password">
                    </div>
                </div>

                <div class="form-group row mb-0">
                    <div class="col-md-6 offset-md-4">
                        <button type="submit" class="btn btn-primary">
                            {{ __('Ubah') }}
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection