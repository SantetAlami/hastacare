@extends('layouts.app')

@section('content')
{{-- <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror"
                                    name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="username" class="col-md-4 col-form-label text-md-right">{{ __('NRP') }}</label>

                            <div class="col-md-6">
                                <input id="username" type="number"
                                    class="form-control @error('username') is-invalid @enderror" name="username"
                                    value="{{ old('username') }}" required autocomplete="username">

                                @error('username')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="unit" class="col-md-4 col-form-label text-md-right">{{ __('Unit') }}</label>

                            <div class="col-md-6">
                                <select name="unit" id="unit" class="form-control @error('unit') is-invalid @enderror"
                                    name="unit" required>
                                    <option value="Polsek">Polsek</option>
                                    <option value="Bagops">Bagops</option>
                                    <option value="Bagsumda">Bagsumda</option>
                                    <option value="Bagren">Bagren</option>
                                    <option value="Satintelkam">Satintelkam</option>
                                    <option value="Satreskrim">Satreskrim</option>
                                    <option value="Satresnarkoba">Satresnarkoba</option>
                                    <option value="Satsabhara">Satsabhara</option>
                                    <option value="Satlantas">Satlantas</option>
                                    <option value="Sattahti">Sattahti</option>
                                    <option value="Siwas">Siwas</option>
                                    <option value="Sikeu">Sikeu</option>
                                    <option value="Sipropam">Sipropam</option>
                                    <option value="Sitipol">Sitipol</option>
                                    <option value="Rumkit">Rumkit</option>
                                    <option value="Brimob Malang">Brimob Malang</option>
                                </select>

                                @error('unit')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="satuan" class="col-md-4 col-form-label text-md-right">{{ __('Satuan') }}</label>

                            <div class="col-md-6">
                                <select name="satuan" id="satuan"
                                    class="form-control @error('satuan') is-invalid @enderror" name="satuan" required>
                                    <option value="RSB Hasta Brata Batu">RSB Hasta Brata Batu</option>
                                    <option value="Polresta Malang Kota">Polresta Malang Kota</option>
                                    <option value="Polres Malang">Polres Malang</option>
                                    <option value="Polres Batu">Polres Batu</option>
                                    <option value="Satbrimob Malang">Satbrimob Malang</option>
                                </select>

                                @error('satuan')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="dinas"
                                class="col-md-4 col-form-label text-md-right">{{ __('Lokasi Dinas') }}</label>

                            <div class="col-md-6">
                                <input id="dinas" type="text" class="form-control @error('dinas') is-invalid @enderror"
                                    name="dinas" value="{{ old('dinas') }}" required autocomplete="dinas">

                                @error('dinas')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="no_hp" class="col-md-4 col-form-label text-md-right">{{ __('No Hp') }}</label>

                            <div class="col-md-6">
                                <input id="no_hp" type="text" class="form-control @error('no_hp') is-invalid @enderror"
                                    name="no_hp" value="{{ old('no_hp') }}" required autocomplete="no_hp">

                                @error('no_hp')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="alamat" class="col-md-4 col-form-label text-md-right">{{ __('Alamat') }}</label>

                            <div class="col-md-6">
                                <input id="alamat" type="text"
                                    class="form-control @error('alamat') is-invalid @enderror" name="alamat"
                                    value="{{ old('alamat') }}" required autocomplete="alamat">

                                @error('alamat')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password"
                                class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password"
                                    class="form-control @error('password') is-invalid @enderror" name="password"
                                    required autocomplete="new-password">

                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm"
                                class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control"
                                    name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div> --}}

<div class="register-box">
    <div class="register-logo">
        <b>Register</b>
    </div>

    <div class="card">
        <div class="card-body register-card-body">
            <p class="login-box-msg">Register a new membership</p>

            <form action="{{ route('register') }}" method="post">
                @csrf
                <div class="input-group mb-3">
                    <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" placeholder="Name" value="{{ old('name') }}" required>
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-user"></span>
                        </div>
                    </div>
                </div>
                <div class="input-group mb-3">
                    <input type="number" name="username" class="form-control @error('username') is-invalid @enderror" placeholder="NRP" value="{{ old('username') }}" required>
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-id-card"></span>
                        </div>
                    </div>
                </div>
                <div class="input-group mb-3">
                    <input type="text" name="pangkat" class="form-control @error('pangkat') is-invalid @enderror" placeholder="pangkat" value="{{ old('pangkat') }}" required>
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-id-card"></span>
                        </div>
                    </div>
                </div>
                <div class="input-group mb-3">
                    <select class="form-control @error('unit') is-invalid @enderror" name="unit" required>
                        <option disabled selected>--Pilih Unit--</option>
                        <option value="Polsek">Polsek</option>
                        <option value="Bagops">Bagops</option>
                        <option value="Bagsumda">Bagsumda</option>
                        <option value="Bagren">Bagren</option>
                        <option value="Satintelkam">Satintelkam</option>
                        <option value="Satreskrim">Satreskrim</option>
                        <option value="Satresnarkoba">Satresnarkoba</option>
                        <option value="Satsabhara">Satsabhara</option>
                        <option value="Satlantas">Satlantas</option>
                        <option value="Sattahti">Sattahti</option>
                        <option value="Siwas">Siwas</option>
                        <option value="Sikeu">Sikeu</option>
                        <option value="Sipropam">Sipropam</option>
                        <option value="Sitipol">Sitipol</option>
                        <option value="Rumkit">Rumkit</option>
                        <option value="Brimob Malang">Brimob Malang</option>
                    </select>
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-id-card"></span>
                        </div>
                    </div>
                </div>
                <div class="input-group mb-3">
                    <select class="form-control @error('satuan') is-invalid @enderror" name="satuan" required>
                        <option disabled selected>--Pilih Satuan--</option>
                        <option value="RSB Hasta Brata Batu">RSB Hasta Brata Batu</option>
                        <option value="Polresta Malang Kota">Polresta Malang Kota</option>
                        <option value="Polres Malang">Polres Malang</option>
                        <option value="Polres Batu">Polres Batu</option>
                        <option value="Satbrimob Malang">Satbrimob Malang</option>
                    </select>
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-id-card"></span>
                        </div>
                    </div>
                </div>
                <div class="input-group mb-3">
                    <input type="text" name="dinas" class="form-control" placeholder="Lokasi Dinas" value="{{ old('dinas') }}" required>
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-map-marker"></span>
                        </div>
                    </div>
                </div>
                <div class="input-group mb-3">
                    <input type="text" name="no_hp" class="form-control" placeholder="No HP" value="{{ old('no_hp') }}" required>
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-phone"></span>
                        </div>
                    </div>
                </div>
                <div class="input-group mb-3">
                    <input type="text" name="alamat" class="form-control" placeholder="Alamat" value="{{ old('alamat') }}" required>
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-home"></span>
                        </div>
                    </div>
                </div>
                <div class="input-group mb-3">
                    <input type="password" name="password" class="form-control" placeholder="Password" required>
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-lock"></span>
                        </div>
                    </div>
                </div>
                <div class="input-group mb-3">
                    <input type="password" name="password_confirmation" class="form-control" placeholder="Confirm  password" required>
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-lock"></span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-8">
                        {{-- <div class="icheck-primary">
                            <input type="checkbox" id="agreeTerms" name="terms" value="agree">
                            <label for="agreeTerms">
                                I agree to the <a href="#">terms</a>
                            </label>
                        </div> --}}
                    </div>
                    <!-- /.col -->
                    <div class="col-4">
                        <button type="submit" class="btn btn-primary btn-block">Register</button>
                    </div>
                    <!-- /.col -->
                </div>
            </form>

            <a href="{{ route('login') }}" class="text-center">I already have a membership</a>
        </div>
        <!-- /.form-box -->
    </div><!-- /.card -->
</div>

@endsection