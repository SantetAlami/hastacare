@extends('layouts.app')

@section('content')
<div class="row col-12">
    <div class="col-lg-3 col-6">
        <!-- small box -->
        <a href="{{ route('riwayat') }}" class="small-box bg-info">
            <div class="inner">
                <h3><i class="fa fa-user"></i></h3>

                <p>Profil Kesehatan</p>
            </div>
            <div class="icon">
                <i class="ion ion-bag"></i>
            </div>
            <div class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></div>
        </a>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-6">
        <!-- small box -->
        <a href="{{ url('ticket_kesehatan') }}" class="small-box bg-success">
            <div class="inner">
                <h3><i class="fa fa-first-aid"></i></h3>

                <p>Kesehatan dan Perawatan</p>
            </div>
            <div class="icon">
                <i class="fa fa-first-aid"></i>
            </div>
            <div class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></div>
        </a>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-6">
        <!-- small box -->
        <a href="{{ url('sarana_dan_pelayanan') }}" class="small-box bg-warning">
            <div class="inner">
                <h3><i class="fa fa-pills"></i></h3>

                <p>Sarana Pelayanan</p>
            </div>
            <div class="icon">
                <i class="fa fa-pills"></i>
            </div>
            <div class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></div>
        </a>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-6">
        <!-- small box -->
        <a href="{{ url('ketersediaan_obat') }}" class="small-box bg-danger">
            <div class="inner">
                <h3><i class="fa fa-vials"></i></h3>

                <p>Ketersediaan Obat</p>
            </div>
            <div class="icon">
                <i class="fa fa-vials"></i>
            </div>
            <div class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></div>
        </a>
    </div>
    <!-- ./col -->
</div>
<div class="col-md-12">
    <div class="card">
        <div class="card-header"><b style="color: red">PIRATA</b></div>

        <div class="card-body">
            @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
            @endif

            {{-- {{ \Illuminate\Foundation\Inspiring::quote() }} --}}
            <b>HAM<span style="color:red">PI</span>RI <span style="color:red">RA</span>WAT AN<span style="color:red">TA</span>R</b>
        </div>
    </div>
</div>
@endsection