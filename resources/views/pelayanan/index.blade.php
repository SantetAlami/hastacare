@extends('layouts.app')

@section('content')
<div class="row col-12">
    <div class="col-lg-3 col-6">
        <!-- small box -->
        <a href="{{ url('jadwal_dokter') }}" class="small-box bg-info">
            <div class="inner">
                <h3><i class="fa fa-user-md"></i></h3>

                <p>Jadwal Dokter</p>
            </div>
            <div class="icon">
                <i class="fa fa-user-md"></i>
            </div>
            <div class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></div>
        </a>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-6">
        <!-- small box -->
        <a href="{{ url('ketersediaan_kamar') }}" class="small-box bg-info">
            <div class="inner">
                <h3><i class="fa fa-first-aid"></i></h3>

                <p>Ketersediaan Kamar</p>
            </div>
            <div class="icon">
                <i class="fa fa-first-aid"></i>
            </div>
            <div class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></div>
        </a>
    </div>
    <!-- ./col -->
    {{-- <div class="col-lg-3 col-6">
        <!-- small box -->
        <a href="{{ url('ticket_pengobatan') }}" class="small-box bg-info">
            <div class="inner">
                <h3><i class="fa fa-pills"></i></h3>

                <p>Layanan Pendukung</p>
            </div>
            <div class="icon">
                <i class="fa fa-pills"></i>
            </div>
            <div class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></div>
        </a>
    </div> --}}
</div>
@endsection