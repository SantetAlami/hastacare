<div class="form-group {{ $errors->has('nama') ? 'has-error' : ''}}">
    <label for="nama" class="control-label">{{ 'Nama' }}</label>
    <input class="form-control" name="nama" type="text" id="nama" value="{{ isset($jadwaldokter->nama) ? $jadwaldokter->nama : ''}}" >
    {!! $errors->first('nama', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('spesialis') ? 'has-error' : ''}}">
    <label for="spesialis" class="control-label">{{ 'Spesialis' }}</label>
    <input class="form-control" name="spesialis" type="text" id="spesialis" value="{{ isset($jadwaldokter->spesialis) ? $jadwaldokter->spesialis : ''}}" >
    {!! $errors->first('spesialis', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('senin') ? 'has-error' : ''}}">
    <label for="senin" class="control-label">{{ 'Senin' }}</label>
    <br>
    <span>Jam pertama</span>
    <div class="row">
        <input class="form-control col-5" name="hari[senin][1]" type="time" id="senin" min="00:00" max="23:59" value="{{ isset($jadwaldokter->senin) ? substr($jadwaldokter->senin,0,5) : ''}}" > - 
        <input class="form-control col-5" name="hari[senin][2]" type="time" id="senin" min="00:00" max="23:59" value="{{ isset($jadwaldokter->senin) ? substr($jadwaldokter->senin,8,5) : ''}}" >
    </div>
    <br>
    <span>Jam kedua</span>
    <div class="row">
        <input class="form-control col-5" name="hari[senin][3]" type="time" id="senin" min="00:00" max="23:59" value="{{ isset($jadwaldokter->senin) ? substr($jadwaldokter->senin,15,5) : ''}}" > - 
        <input class="form-control col-5" name="hari[senin][4]" type="time" id="senin" min="00:00" max="23:59" value="{{ isset($jadwaldokter->senin) ? substr($jadwaldokter->senin,23,5) : ''}}" >
    </div>
    {!! $errors->first('senin', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('selasa') ? 'has-error' : ''}}">
    <label for="selasa" class="control-label">{{ 'Selasa' }}</label>
    <br>
    <span>Jam pertama</span>
    <div class="row">
        <input class="form-control col-5" name="hari[selasa][1]" type="time" id="selasa" min="00:00" max="23:59" value="{{ isset($jadwaldokter->selasa) ? substr($jadwaldokter->selasa,0,5) : ''}}" > - 
        <input class="form-control col-5" name="hari[selasa][2]" type="time" id="selasa" min="00:00" max="23:59" value="{{ isset($jadwaldokter->selasa) ? substr($jadwaldokter->selasa,8,5) : ''}}" >
    </div>
    <br>
    <span>Jam kedua</span>
    <div class="row">
        <input class="form-control col-5" name="hari[selasa][3]" type="time" id="selasa" min="00:00" max="23:59" value="{{ isset($jadwaldokter->selasa) ? substr($jadwaldokter->selasa,15,5) : ''}}" > - 
        <input class="form-control col-5" name="hari[selasa][4]" type="time" id="selasa" min="00:00" max="23:59" value="{{ isset($jadwaldokter->selasa) ? substr($jadwaldokter->selasa,23,5) : ''}}" >
    </div>
    {!! $errors->first('selasa', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('rabu') ? 'has-error' : ''}}">
    <label for="rabu" class="control-label">{{ 'Rabu' }}</label>
    <br>
    <span>Jam pertama</span>
    <div class="row">
        <input class="form-control col-5" name="hari[rabu][1]" type="time" id="rabu" min="00:00" max="23:59" value="{{ isset($jadwaldokter->rabu) ? substr($jadwaldokter->rabu,0,5) : ''}}" > - 
        <input class="form-control col-5" name="hari[rabu][2]" type="time" id="rabu" min="00:00" max="23:59" value="{{ isset($jadwaldokter->rabu) ? substr($jadwaldokter->rabu,8,5) : ''}}" >
    </div>
    <br>
    <span>Jam kedua</span>
    <div class="row">
        <input class="form-control col-5" name="hari[rabu][3]" type="time" id="rabu" min="00:00" max="23:59" value="{{ isset($jadwaldokter->rabu) ? substr($jadwaldokter->rabu,15,5) : ''}}" > - 
        <input class="form-control col-5" name="hari[rabu][4]" type="time" id="rabu" min="00:00" max="23:59" value="{{ isset($jadwaldokter->rabu) ? substr($jadwaldokter->rabu,23,5) : ''}}" >
    </div>
    {!! $errors->first('rabu', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('kamis') ? 'has-error' : ''}}">
    <label for="kamis" class="control-label">{{ 'Kamis' }}</label>
    <br>
    <span>Jam pertama</span>
    <div class="row">
        <input class="form-control col-5" name="hari[kamis][1]" type="time" id="kamis" min="00:00" max="23:59" value="{{ isset($jadwaldokter->kamis) ? substr($jadwaldokter->kamis,0,5) : ''}}" > - 
        <input class="form-control col-5" name="hari[kamis][2]" type="time" id="kamis" min="00:00" max="23:59" value="{{ isset($jadwaldokter->kamis) ? substr($jadwaldokter->kamis,8,5) : ''}}" >
    </div>
    <br>
    <span>Jam kedua</span>
    <div class="row">
        <input class="form-control col-5" name="hari[kamis][3]" type="time" id="kamis" min="00:00" max="23:59" value="{{ isset($jadwaldokter->kamis) ? substr($jadwaldokter->kamis,15,5) : ''}}" > - 
        <input class="form-control col-5" name="hari[kamis][4]" type="time" id="kamis" min="00:00" max="23:59" value="{{ isset($jadwaldokter->kamis) ? substr($jadwaldokter->kamis,23,5) : ''}}" >
    </div>
    {!! $errors->first('kamis', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('jumat') ? 'has-error' : ''}}">
    <label for="jumat" class="control-label">{{ 'Jumat' }}</label>
    <br>
    <span>Jam pertama</span>
    <div class="row">
        <input class="form-control col-5" name="hari[jumat][1]" type="time" id="jumat" min="00:00" max="23:59" value="{{ isset($jadwaldokter->jumat) ? substr($jadwaldokter->jumat,0,5) : ''}}" > - 
        <input class="form-control col-5" name="hari[jumat][2]" type="time" id="jumat" min="00:00" max="23:59" value="{{ isset($jadwaldokter->jumat) ? substr($jadwaldokter->jumat,8,5) : ''}}" >
    </div>
    <br>
    <span>Jam kedua</span>
    <div class="row">
        <input class="form-control col-5" name="hari[jumat][3]" type="time" id="jumat" min="00:00" max="23:59" value="{{ isset($jadwaldokter->jumat) ? substr($jadwaldokter->jumat,15,5) : ''}}" > - 
        <input class="form-control col-5" name="hari[jumat][4]" type="time" id="jumat" min="00:00" max="23:59" value="{{ isset($jadwaldokter->jumat) ? substr($jadwaldokter->jumat,23,5) : ''}}" >
    </div>
    {!! $errors->first('jumat', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('sabtu') ? 'has-error' : ''}}">
    <label for="sabtu" class="control-label">{{ 'Sabtu' }}</label>
    <br>
    <span>Jam pertama</span>
    <div class="row">
        <input class="form-control col-5" name="hari[sabtu][1]" type="time" id="sabtu" min="00:00" max="23:59" value="{{ isset($jadwaldokter->sabtu) ? substr($jadwaldokter->sabtu,0,5) : ''}}" > - 
        <input class="form-control col-5" name="hari[sabtu][2]" type="time" id="sabtu" min="00:00" max="23:59" value="{{ isset($jadwaldokter->sabtu) ? substr($jadwaldokter->sabtu,8,5) : ''}}" >
    </div>
    <br>
    <span>Jam kedua</span>
    <div class="row">
        <input class="form-control col-5" name="hari[sabtu][3]" type="time" id="sabtu" min="00:00" max="23:59" value="{{ isset($jadwaldokter->sabtu) ? substr($jadwaldokter->sabtu,15,5) : ''}}" > - 
        <input class="form-control col-5" name="hari[sabtu][4]" type="time" id="sabtu" min="00:00" max="23:59" value="{{ isset($jadwaldokter->sabtu) ? substr($jadwaldokter->sabtu,23,5) : ''}}" >
    </div>
    {!! $errors->first('sabtu', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('minggu') ? 'has-error' : ''}}">
    <label for="minggu" class="control-label">{{ 'Minggu' }}</label>
    <br>
    <span>Jam pertama</span>
    <div class="row">
        <input class="form-control col-5" name="hari[minggu][1]" type="time" id="minggu" min="00:00" max="23:59" value="{{ isset($jadwaldokter->minggu) ? substr($jadwaldokter->minggu,0,5) : ''}}" > - 
        <input class="form-control col-5" name="hari[minggu][2]" type="time" id="minggu" min="00:00" max="23:59" value="{{ isset($jadwaldokter->minggu) ? substr($jadwaldokter->minggu,8,5) : ''}}" >
    </div>
    <br>
    <span>Jam kedua</span>
    <div class="row">
        <input class="form-control col-5" name="hari[minggu][3]" type="time" id="minggu" min="00:00" max="23:59" value="{{ isset($jadwaldokter->minggu) ? substr($jadwaldokter->minggu,15,5) : ''}}" > - 
        <input class="form-control col-5" name="hari[minggu][4]" type="time" id="minggu" min="00:00" max="23:59" value="{{ isset($jadwaldokter->minggu) ? substr($jadwaldokter->minggu,23,5) : ''}}" >
    </div>
    {!! $errors->first('minggu', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>
