@extends('layouts.app')

@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="card-header">Edit JadwalDokter #{{ $jadwaldokter->id }}</div>
        <div class="card-body">
            <a href="{{ url('/jadwal_dokter') }}" title="Back"><button class="btn btn-warning btn-sm"><i
                        class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
            <br />
            <br />

            @if ($errors->any())
            <ul class="alert alert-danger">
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
            @endif

            <form method="POST" action="{{ url('/jadwal_dokter/' . $jadwaldokter->id) }}" accept-charset="UTF-8"
                class="form-horizontal" enctype="multipart/form-data">
                {{ method_field('PATCH') }}
                {{ csrf_field() }}

                @include ('jadwal_dokter.form', ['formMode' => 'edit'])

            </form>

        </div>
    </div>
</div>
@endsection