@extends('layouts.app')

@section('content')
<div class="col-md-12">
    <div class="card card-primary card-outline">
        <div class="card-header">Jadwal dokter</div>
        <div class="card-body">
            <a href="{{ url('/sarana_dan_pelayanan') }}" title="Back"><button class="btn btn-outline-warning btn-sm"><i
                class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
            @if (Auth::user()->admin == 1)
            <a href="{{ url('/jadwal_dokter/create') }}" class="btn btn-success btn-sm" title="Add New JadwalDokter">
                <i class="fa fa-plus" aria-hidden="true"></i> Add New
            </a>
            @endif

            <form method="GET" action="{{ url('/jadwal_dokter') }}" accept-charset="UTF-8"
                class="form-inline my-2 my-lg-0 float-right" role="search">
                <div class="input-group">
                    <input type="text" class="form-control" name="search" placeholder="Search..."
                        value="{{ request('search') }}">
                    <span class="input-group-append">
                        <button class="btn btn-secondary" type="submit">
                            <i class="fa fa-search"></i>
                        </button>
                    </span>
                </div>
            </form>

            <br />
            <br />
            <div class="table-responsive table-striped table-bordered">
                <table class="table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nama Dokter</th>
                            <th>Spesialis</th>
                            <th>Senin</th>
                            <th>Selasa</th>
                            <th>Rabu</th>
                            <th>Kamis</th>
                            <th>Jumat</th>
                            <th>Sabtu</th>
                            <th>Minggu</th>
                            @if (Auth::user()->admin == 1)
                            <th>Actions</th>
                            @endif
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $sp = [
                                "Penyakit Dalam" => "yellow",
                                "Penyakit Syaraf" => "blue",
                                "Orthopaedi" => "gray",
                                "Anak" => "green",
                                "Kandungan" => "pink",
                                "THT" => "lightblue",
                                "Bedah" => "maroon",
                                "Kulit Kelamin" => "clay",
                                "Mata" => "bluesky",
                                "Paru" => "lightgreen",
                                "Gigi" => "white",
                                "Urologi" => "deepblue",
                            ];
                        @endphp
                        @foreach($jadwaldokter as $item)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $item->nama }}</td>
                            <td style="background-color: {{ $sp[$item->spesialis] }}; color: {{ in_array($item->spesialis, ["Anak","Bedah","Penyakit Syaraf"]) ? 'white' : 'black' }}">{{ $item->spesialis }}</td>
                            <td style="white-space: pre-line; {{ empty($item->senin) ? 'background-color: red' : '' }}">{{ $item->senin }}</td>
                            <td style="white-space: pre-line; {{ empty($item->selasa) ? 'background-color: red' : '' }}">{{ $item->selasa }}</td>
                            <td style="white-space: pre-line; {{ empty($item->rabu) ? 'background-color: red' : '' }}">{{ $item->rabu }}</td>
                            <td style="white-space: pre-line; {{ empty($item->kamis) ? 'background-color: red' : '' }}">{{ $item->kamis }}</td>
                            <td style="white-space: pre-line; {{ empty($item->jumat) ? 'background-color: red' : '' }}">{{ $item->jumat }}</td>
                            <td style="white-space: pre-line; {{ empty($item->sabtu) ? 'background-color: red' : '' }}">{{ $item->sabtu }}</td>
                            <td style="white-space: pre-line; {{ empty($item->minggu) ? 'background-color: red' : '' }}">{{ $item->minggu }}</td>
                            @if (Auth::user()->admin == 1)
                            <td>
                                <a href="{{ url('/jadwal_dokter/' . $item->id) }}" title="View JadwalDokter"><button
                                        class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i></button></a>
                                <a href="{{ url('/jadwal_dokter/' . $item->id . '/edit') }}"
                                    title="Edit JadwalDokter"><button class="btn btn-primary btn-sm"><i
                                            class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a>

                                <form method="POST" action="{{ url('/jadwal_dokter' . '/' . $item->id) }}"
                                    accept-charset="UTF-8" style="display:inline">
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}
                                    <button type="submit" class="btn btn-danger btn-sm" title="Delete JadwalDokter"
                                        onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash"
                                            aria-hidden="true"></i></button>
                                </form>
                            </td>
                            @endif
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="pagination-wrapper"> {!! $jadwaldokter->appends(['search' =>
                    Request::get('search')])->render() !!} </div>
            </div>

        </div>
    </div>
</div>
@endsection