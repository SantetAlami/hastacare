<?php

namespace App\Events;

use App\User;
use App\Models\Panic;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class PanicSent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * User that sent the message
     *
     * @var User
     */
    public $user;

    /**
     * Panic details
     *
     * @var Panic
     */
    public $panic;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(User $user, Panic $panic)
    {
        // $user['ticket'] = $ticket;
        $this->user = $user;
        $this->panic = $panic;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('chat');
    }
}