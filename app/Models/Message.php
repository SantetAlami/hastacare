<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
/**
 * The database table used by the model.
 *
 * @var string
 */
    protected $table = 'messages';

/**
 * The database primary key value.
 *
 * @var string
 */
    protected $primaryKey = 'id';

/**
 * Attributes that should be mass-assignable.
 *
 * @var array
 */
    protected $fillable = ['user_id', 'ticket_id', 'message', 'type', 'read'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
