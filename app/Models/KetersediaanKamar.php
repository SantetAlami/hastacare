<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class KetersediaanKamar extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'ketersediaan_kamars';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['ruang', 'tempat_tidur', 'terisi', 'total'];

    
}
