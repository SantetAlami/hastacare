<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Panic extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'panics';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'status'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
