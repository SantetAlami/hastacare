<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Odontogram extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'odontograms';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'no', 'status', 'warna'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
