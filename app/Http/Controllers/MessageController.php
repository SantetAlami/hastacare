<?php

namespace App\Http\Controllers;

use App\Events\MessageSent;
use App\Http\Controllers\Controller;
use App\Models\Message;
use App\Models\Ticket;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MessageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $message = Message::where('user_id', 'LIKE', "%$keyword%")
                ->orWhere('ticket_id', 'LIKE', "%$keyword%")
                ->orWhere('message', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $message = Message::latest()->paginate($perPage);
        }

        return view('message.index', compact('message'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('message.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {

        $requestData = $request->all();

        $requestData['user_id'] = Auth::user()->id;

        $message = Message::create($requestData);
        if (Auth::user()->admin == 1) {
            Ticket::where('id', $requestData['ticket_id'])->where('status','<>','2')->update(['status' => 1]);
        }
        $ticket = Ticket::where('id', $requestData['ticket_id'])->first();
        broadcast(new MessageSent(Auth::user(),  $message, $ticket));

        // return redirect('message')->with('flash_message', 'Message added!');
        return redirect('ticket_kesehatan/' . $requestData['ticket_id'])->with('flash_message', 'Ticket added!');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function storeObat(Request $request)
    {

        $requestData = $request->all();

        $requestData['user_id'] = Auth::user()->id;

        $message = Message::create($requestData);
        if (Auth::user()->admin == 1) {
            Ticket::where('id', $requestData['ticket_id'])->where('status','<>','2')->update(['status' => 1]);
        }
        $ticket = Ticket::where('id', $requestData['ticket_id'])->first();
        broadcast(new MessageSent(Auth::user(),  $message, $ticket));

        // return redirect('message')->with('flash_message', 'Message added!');
        return redirect('ticket_pengobatan/' . $requestData['ticket_id'])->with('flash_message', 'Ticket added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $message = Message::findOrFail($id);

        return view('message.show', compact('message'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $message = Message::findOrFail($id);

        return view('message.edit', compact('message'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {

        $requestData = $request->all();
        $requestData['user_id'] = Auth::user()->id;

        $message = Message::where(['id' => $id, 'user_id' => Auth::user()->id])->first();
        $message->update($requestData);

        return redirect('message')->with('flash_message', 'Message updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Message::destroy($id);

        return redirect('message')->with('flash_message', 'Message deleted!');
    }

    /**
     * Fetch all messages
     *
     * @return Message
     */
    public function fetchMessages()
    {
        return Message::with('ticket')->get();
    }

    /**
     * Persist message to database
     *
     * @param  Request $request
     * @return Response
     */
    public function sendMessage(Request $request)
    {
        $requestData = $request->all();

        $requestData['user_id'] = Auth::user()->id;

        $message = Message::create($requestData);
        if (Auth::user()->admin == 1) {
            Ticket::where('id', $requestData['ticket_id'])->update(['status' => 1]);
            $ticket = Ticket::where('id', $requestData['ticket_id'])->first();
            // return ['status' => 'Message Sent! '. $ticket];
            Message::where('ticket_id', $requestData['ticket_id'])->where('user_id', $ticket->user_id)->update(['read' => 1]);
        }else{
            $ticket = Ticket::where('id', $requestData['ticket_id'])->first();
            Message::where('ticket_id', $requestData['ticket_id'])->where('user_id', '<>', Auth::user()->id)->update(['read' => 1]);
        }
        broadcast(new MessageSent(Auth::user(),  $message, $ticket));

        // return redirect('message')->with('flash_message', 'Message added!');
        // return redirect('ticket/' . $requestData['ticket_id'])->with('flash_message', 'Ticket added!');
        return ['status' => 'Message Sent!'];
    }
}
