<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Riwayat;
use App\Models\Odontogram;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use File;

class RiwayatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $user = Auth::user();
        // $keyword = $request->get('search');
        $perPage = 250;

        // if (!empty($keyword)) {
        //     $riwayat = Riwayat::where('user_id', 'LIKE', "%$keyword%")
        //         ->orWhere('judul', 'LIKE', "%$keyword%")
        //         ->orWhere('deskripsi', 'LIKE', "%$keyword%")
        //         ->latest()->paginate($perPage);
        // } else {
        $riwayat = Riwayat::where('user_id', Auth::user()->id)->orderby('tgl')->paginate($perPage);
        $odo = Odontogram::where('user_id', $user->id)->get();

        $odontogram = [];
        foreach ($odo as $key => $value) {
            $odontogram[$value->no] = $value->warna;
        }
        // }
        // dd($odontogram);

        return view('riwayat.index', compact('riwayat', 'user', 'odontogram'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('riwayat.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {

        $requestData = $request->all();
        if(Auth::user()->admin != 1){
            $requestData['user_id'] = Auth::user()->id;
        }


        Riwayat::create($requestData);

        if(Auth::user()->admin == 1 && $requestData['user_id'] != Auth::user()->id){
            return redirect('master/user/' . $requestData['user_id'])->with('flash_message', 'Riwayat added!');
        }
        return redirect('profil_kesehatan')->with('flash_message', 'Riwayat added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $riwayat = Riwayat::findOrFail($id);

        return view('riwayat.show', compact('riwayat'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $riwayat = Riwayat::findOrFail($id);

        return view('riwayat.edit', compact('riwayat'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {

        $requestData = $request->all();
        if(Auth::user()->admin != 1 || empty($requestData['user_id'])){
            $requestData['user_id'] = Auth::user()->id;
        }

        // where(['id' => $id, 'user_id' => Auth::user()->id])->
        $riwayat = Riwayat::findOrFail($id);
        $riwayat->update($requestData);

        if(Auth::user()->admin == 1 && $requestData['user_id'] != Auth::user()->id){
            return redirect('master/user/' . $requestData['user_id'])->with('flash_message', 'Riwayat updated!');
        }
        return redirect('profil_kesehatan')->with('flash_message', 'Riwayat updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Riwayat::destroy($id);

        return redirect('profil_kesehatan')->with('flash_message', 'Riwayat deleted!');
    }

    public function ubah_profil(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'password' => ['string', 'min:6', 'confirmed', 'nullable'],
            'unit' => ['required', 'string'],
            'no_hp' => ['required', 'string', 'max:15'],
            'alamat' => ['required', 'string'],
        ]);

        $requestData = $request->all();
        unset($requestData['username']);
        unset($requestData['satuan']);
        unset($requestData['password_confirmation']);

        if (empty($requestData['password'])) {
            unset($requestData['password']);
        } else {
            $requestData['password'] = Hash::make($requestData['password']);
        }

        $request->user()->update(
            $requestData
        );

        return redirect('profil_kesehatan')->with('flash_message', 'Profil terupdate!');
    }

    public function odontogram(Request $request, $id = 0)
    {
        $requestData = $request->all();
        if(Auth::user()->admin != 1 || $id == 0){
            $id = Auth::user()->id;
        }
        $status = explode('|', $requestData["status"]);
        Odontogram::updateOrCreate(
            [
                "user_id" => $id,
                "no" => $requestData["no"],
            ],
            [
                "status" =>  $status[1],
                "warna" =>  $status[0],
            ]
        );

        if(Auth::user()->id != $id && $id != 0){
            return redirect('master/user/' . $id)->with('flash_message', 'Profil terupdate!');
        }
        return redirect('profil_kesehatan')->with('flash_message', 'Profil terupdate!');
    }

    public function upload_foto_gigi(Request $request, $id = 0)
    {
        try {
            if ($request->hasFile('up')) {
                if(Auth::user()->admin != 1 || $id == 0){
                    $id = Auth::user()->id;
                }

                $extensions = ["gif", "jpg", "jpeg", "png", "doc", "docx", "odt", "pdf", "tex", "txt", "rtf", "wps", "wks", "wpd"];
                if (!in_array($request->file('up')->getClientOriginalExtension(), $extensions)) {
                    return redirect('profil_kesehatan')->with('error_message', 'extention file not allowed');
                }
                $uuid = (string) Str::uuid();
                $request->file('up')->move("foto_gigi/", $uuid . $request->file('up')->getClientOriginalName());
                $res = $uuid . $request->file('up')->getClientOriginalName();

                $file_path = public_path("foto_gigi/" . Auth::user()->foto_gigi);
                if(File::exists($file_path)) File::delete($file_path);
                
                User::where('id', $id)->update(["foto_gigi" => $res]);
                if(Auth::user()->id != $id && $id != 0){
                    return redirect('master/user/' . $id)->with('flash_message', 'Upload selesai!');
                }
                return redirect('profil_kesehatan')->with('flash_message', 'Upload selesai');
            }
            return redirect('profil_kesehatan')->with('error_message', 'Upload gagal');
        } catch (\Throwable $e) {
            return redirect('profil_kesehatan')->with('error_message', $e->getMessage());
        }
    }
}
