<?php

namespace App\Http\Controllers;

use App\Events\PanicSent;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Panic;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PanicController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        // $keyword = $request->get('search');
        $perPage = 25;

        // if (!empty($keyword)) {
            $panic = Panic::where('user_id', 'LIKE', Auth::user()->admin == 1 ? "%%" : Auth::user()->id)
                ->latest()->paginate($perPage);
        // } else {
        //     $panic = Panic::latest()->paginate($perPage);
        // }

        return view('panic.index', compact('panic'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('panic.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        $requestData["user_id"] = Auth::user()->id;
        $requestData["status"] = 0;
        
        $panic = Panic::create($requestData);
        broadcast(new PanicSent(Auth::user(),  $panic));

        return redirect('panic');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $panic = Panic::findOrFail($id);

        return view('panic.show', compact('panic'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $panic = Panic::findOrFail($id);

        return view('panic.edit', compact('panic'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $panic = Panic::findOrFail($id);
        $panic->update($requestData);

        return redirect('panic')->with('flash_message', 'Panic updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        // Panic::destroy($id);
        $panic = Panic::findOrFail($id);
        $panic->update(['status' => '1']);

        // dd($panic);
        return redirect('panic')->with('flash_message', 'Panic deleted!');
    }
}
