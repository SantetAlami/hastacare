<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\JadwalDokter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class JadwalDokterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $jadwaldokter = JadwalDokter::where('nama', 'LIKE', "%$keyword%")
                ->orWhere('spesialis', 'LIKE', "%$keyword%")
                ->orWhere('senin', 'LIKE', "%$keyword%")
                ->orWhere('selasa', 'LIKE', "%$keyword%")
                ->orWhere('rabu', 'LIKE', "%$keyword%")
                ->orWhere('kamis', 'LIKE', "%$keyword%")
                ->orWhere('jumat', 'LIKE', "%$keyword%")
                ->orWhere('sabtu', 'LIKE', "%$keyword%")
                ->orWhere('minggu', 'LIKE', "%$keyword%")
                ->orderby('spesialis')->orderby('nama')->paginate($perPage);
        } else {
            $jadwaldokter = JadwalDokter::orderby('spesialis')->orderby('nama')->paginate($perPage);
        }

        return view('jadwal_dokter.index', compact('jadwaldokter'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        if(Auth::user()->admin != 1){
            abort(403);
        }

        return view('jadwal_dokter.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        if(Auth::user()->admin == 1){
            $requestData = $request->all();
            
            JadwalDokter::create($requestData);

            return redirect('jadwal_dokter')->with('flash_message', 'JadwalDokter added!');
        }
        abort(403);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        if(Auth::user()->admin != 1){
            abort(403);
        }
        $jadwaldokter = JadwalDokter::findOrFail($id);

        return view('jadwal_dokter.show', compact('jadwaldokter'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        if(Auth::user()->admin != 1){
            abort(403);
        }
        $jadwaldokter = JadwalDokter::findOrFail($id);

        return view('jadwal_dokter.edit', compact('jadwaldokter'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        if(Auth::user()->admin == 1){
            $requestData = $request->all();
            foreach ($requestData["hari"] as $key => $item) {
                $requestData[$key] = ($item[1] ?? '') . (isset($item[1]) ? ' - ' : '') . ($item[2] ?? '') . (isset($item[3]) ? "\n" : '') . ($item[3] ?? '') . (isset($item[3]) ? ' - ' : '') . ($item[4] ?? '');
            }
            
            $jadwaldokter = JadwalDokter::findOrFail($id);
            $jadwaldokter->update($requestData);
    
            return redirect('jadwal_dokter')->with('flash_message', 'JadwalDokter updated!');
        }
        abort(403);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        if(Auth::user()->admin != 1){
            abort(403);
        }
        JadwalDokter::destroy($id);

        return redirect('jadwal_dokter')->with('flash_message', 'JadwalDokter deleted!');
    }
}
