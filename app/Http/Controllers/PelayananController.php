<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PelayananController extends Controller
{
    public function jadwal(Request $request)
    {
        $res = DB::table('v_monitor')->select('*')->get();

        $name = [];
        $total = [];
        $chating = [];
        foreach ($res as $key => $item) {
            $name[] = $item->name;
            $total[] = $item->keaktifan_total;
            $chating[] = $item->total_chating;
        }

        return view('pelayanan.jadwal_dokter', compact('name', 'total', 'chating'));
    }
}
