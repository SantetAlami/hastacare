<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function panic(Request $request)
    {
        # code...
    }

    public function monitor(Request $request)
    {
        $res = DB::table('v_monitor')->select('*')->get();

        $name = [];
        $total = [];
        $chating = [];
        foreach ($res as $key => $item) {
            $name[] = $item->name;
            $total[] = $item->keaktifan_total;
            $chating[] = $item->total_chating;
        }

        return view('monitor', compact('name', 'total', 'chating'));
    }
}
