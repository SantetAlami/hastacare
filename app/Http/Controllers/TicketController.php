<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Riwayat;
use App\Models\Ticket;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TicketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $status = $request->get('status');
        $perPage = 25;

        $ticket = Ticket::where('user_id', 'LIKE', Auth::user()->admin == 1 ? "%%" : Auth::user()->id)
                            ->where('status', 'like', $status ?? '%%')
                            ->where('type', 'like', '0')
                            ->orderBy('status')
                            ->paginate($perPage);

        // dd($ticket[1]->user->name);
        return view('ticket.index', compact('ticket'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('ticket.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        $requestData['user_id'] = Auth::user()->id;
        $requestData['type'] = 0;

        Ticket::create($requestData);

        return redirect('ticket_kesehatan')->with('flash_message', 'Ticket added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $ticket = Ticket::findOrFail($id);
        foreach ($ticket->message as $key => $item) {
            $ticket->message[$key]->user = User::findOrFail($item->user_id);
        }

        $riwayat = Riwayat::where('user_id', $ticket->user_id)->get();
        // dd($ticket->message);

        return view('ticket.show', compact('ticket','riwayat'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $ticket = Ticket::findOrFail($id);

        return view('ticket.edit', compact('ticket'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        $requestData['user_id'] = Auth::user()->id;
        $requestData['type'] = 0;
        
        $ticket = Ticket::where(['id' => $id, 'user_id' => Auth::user()->id])->where('status', '<>', 2)->first();
        $ticket->update($requestData);

        return redirect('ticket_kesehatan')->with('flash_message', 'Ticket updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        if(Auth::user()->admin == 1){
            $ticket = Ticket::where(['id' => $id]);
        }else {
            $ticket = Ticket::where(['id' => $id, 'user_id' => Auth::user()->id]);
        }
        $ticket->update(['status' => 2]);

        return redirect('ticket_kesehatan')->with('flash_message', 'Ticket deleted!');
    }
}
