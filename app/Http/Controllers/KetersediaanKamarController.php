<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\KetersediaanKamar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class KetersediaanKamarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $ketersediaankamar = KetersediaanKamar::where('ruang', 'LIKE', "%$keyword%")
                ->orWhere('tempat_tidur', 'LIKE', "%$keyword%")
                ->orWhere('terisi', 'LIKE', "%$keyword%")
                ->orWhere('total', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $ketersediaankamar = KetersediaanKamar::latest()->paginate($perPage);
        }

        return view('ketersediaan_kamar.index', compact('ketersediaankamar'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        if(Auth::user()->admin != 1){
            abort(403);
        }
        return view('ketersediaan_kamar.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        if(Auth::user()->admin != 1){
            abort(403);
        }

        $requestData = $request->all();
        
        KetersediaanKamar::create($requestData);

        return redirect('ketersediaan_kamar')->with('flash_message', 'KetersediaanKamar added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        if(Auth::user()->admin != 1){
            abort(403);
        }
        $ketersediaankamar = KetersediaanKamar::findOrFail($id);

        return view('ketersediaan_kamar.show', compact('ketersediaankamar'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        if(Auth::user()->admin != 1){
            abort(403);
        }
        $ketersediaankamar = KetersediaanKamar::findOrFail($id);

        return view('ketersediaan_kamar.edit', compact('ketersediaankamar'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        if(Auth::user()->admin != 1){
            abort(403);
        }
        
        $requestData = $request->all();
        
        $ketersediaankamar = KetersediaanKamar::findOrFail($id);
        $ketersediaankamar->update($requestData);

        return redirect('ketersediaan_kamar')->with('flash_message', 'KetersediaanKamar updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        if(Auth::user()->admin != 1){
            abort(403);
        }
        KetersediaanKamar::destroy($id);

        return redirect('ketersediaan_kamar')->with('flash_message', 'KetersediaanKamar deleted!');
    }
}
