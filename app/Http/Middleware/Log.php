<?php

namespace App\Http\Middleware;

use App\Models\Log as ModelsLog;
use Closure;

class Log
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = json_encode($request->user());
        $path = json_encode($request->path());
        $method = json_encode($request->method());
        $header = json_encode($request->header());
        $all = json_encode($request->all());
        // dd($user,$path,$method,$header,$all);
        ModelsLog::create(['user_id' => $request->user()->id,'user' => $user,'path' => $path,'method' => $method,'header' => $header,'body' => $all]);
        return $next($request);
    }
}
