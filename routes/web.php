<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

use Illuminate\Support\Facades\Request;

Route::get('/', function () {
    // return view('welcome');
    return redirect('home');
});

Route::middleware(['auth','log'])->group(function () {
    Route::resource('profil_kesehatan', 'RiwayatController', [
        'names' => [
            'index' => 'riwayat',
            'create' => 'riwayat.create',
            'store' => 'riwayat.store',
            'show' => 'riwayat.show',
            'edit' => 'riwayat.edit',
            'update' => 'riwayat.update',
            'delete' => 'riwayat.delete',
        ],
    ]);
    Route::resource('ticket_kesehatan', 'TicketController');
    Route::resource('ticket_pengobatan', 'TicketObatController');
    Route::resource('message', 'MessageController', [
        'names' => [
            'index' => 'message',
            'create' => 'message.create',
            'store' => 'message.store',
            'show' => 'message.show',
            'edit' => 'message.edit',
            'update' => 'message.update',
            'delete' => 'message.delete',
        ],
    ]);
    Route::resource('ketersediaan_obat', 'ObatController');

    Route::get('messages', 'MessageController@fetchMessages');
    Route::post('messages', 'MessageController@sendMessage');
    Route::post('messages_obat', 'MessageController@storeObat');

    Route::get('/Profil_kesehatan/ubah_profil', function (Request $request)
    {
        return view('auth.ubah');
    });
    Route::post('ubah_profil', 'RiwayatController@ubah_profil');
    Route::post('ubah_odontogram/{id?}', 'RiwayatController@odontogram');
    Route::post('upload_foto_gigi/{id?}', 'RiwayatController@upload_foto_gigi');
    
    Route::resource('panic', 'PanicController');
    
    Route::resource('master/user', 'UserController');

    Route::get('/sarana_dan_pelayanan', function () {
        // return view('welcome');
        return view('pelayanan.index');
    });
    Route::get('/monitor', 'HomeController@monitor')->middleware('admin');

    Route::resource('jadwal_dokter', 'JadwalDokterController');
});
Route::get('/home', 'HomeController@index')->name('home');
Auth::routes();


Route::resource('ketersediaan_kamar', 'KetersediaanKamarController');