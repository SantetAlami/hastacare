<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateKetersediaanKamarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ketersediaan_kamars', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('ruang')->nullable();
            $table->integer('tempat_tidur')->nullable();
            $table->integer('terisi')->nullable();
            $table->integer('total')->nullable();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ketersediaan_kamars');
    }
}
